import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
//import { MaterialModule } from '@angular/material';
//EditEmpresaModule
import { CompanyModule } from './pages/component/company/company.module';
import { UserModule } from './pages/component/user/user.module';
import { RolModule } from './pages/component/rol/rol.module';
import { CargoModule } from './pages/component/cargo/cargo.module';
//
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

import { NavigationComponent } from './shared/header-navigation/navigation.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { BreadcrumbComponent } from './shared/breadcrumb/breadcrumb.component';
import { AppComponent } from './app.component';
import { LandigpageComponent } from './landigpage/landigpage.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { RightSidebarComponent } from './shared/right-sidebar/rightsidebar.component';

import { GuardService } from './services/guard.service';
import { AuthService } from './services/auth.service';
import { CompanyService } from './services/company.service';
import { CargoService } from './services/cargo.service';
import { UserService } from './services/user.service';
import { RolService } from './services/rol.service';
import { CategoryService } from './services/category.service';
import { UploadService } from './services/upload.service';
import { LoginComponent } from './login/login.component';
import { SingupComponent } from './singup/singup.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RecoverComponent } from './recover/recover.component';
import {CategoryModule} from "./pages/component/category/category.module";
import {AppointmentModule} from "./pages/component/appointments/appointment.module";
import {AppointmentService} from "./services/appointments.service";
import {BehaviourListService} from "./services/behaviourList.service";
import {BehaviourListComponent} from "./pages/component/behaviourList/behaviourList.component";
import {BehaviourListModule} from "./pages/component/behaviourList/behaviourList.module";
import {GroupModule} from "./pages/component/group/group.module";
import {GroupService} from "./services/group.service";
import {AreaModule} from "./pages/component/area/area.module";
import {AreaService} from "./services/area.service";
import {QScaleModule} from "./pages/component/QScale/qScale.module";
import {QualificationScaleService} from "./services/qualificationScale.service";
export const firebaseConfig = {
  apiKey: "AIzaSyCivkyTjlf408nVMlYIjY7b7wUqtu_Tp-4",
  authDomain: "performancecoana.firebaseapp.com",
  databaseURL: "https://performancecoana.firebaseio.com",
  projectId: "performancecoana",
  storageBucket: "performancecoana.appspot.com",
  messagingSenderId: "118982844140"
};
const appRoutes: Routes = [
  {
    path: '',
    loadChildren: './pages/starter/starter.module#StarterModule',
     //canActivate:[GuardService]
  },{
    path: 'profile',
    loadChildren: './pages/profile/profile.module#ProfileModule'
  },{
    path: 'business',
    loadChildren: './pages/business/business.module#BusinessModule'
  },{
    path: 'recover',
    component: RecoverComponent
  },{
    path: 'login',
    component: LoginComponent
  },{
    path: 'singup',
    component: SingupComponent
  },{
    path: 'not-found',
    component: NotFoundComponent
   },{
     path: '**',
     redirectTo: 'not-found', pathMatch: 'full'
   }
];
@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    BreadcrumbComponent,
    SidebarComponent,
    RightSidebarComponent,
    LandigpageComponent,
    LoginComponent,
    SingupComponent,
    NotFoundComponent,
    RecoverComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MultiselectDropdownModule,
    CommonModule,
    ReactiveFormsModule,
    HttpModule,
    CompanyModule,
    UserModule,
    AreaModule,
    CategoryModule,
    GroupModule,
    AppointmentModule,
    BehaviourListModule,
    QScaleModule,
    RolModule,
    CargoModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthService,
    GroupService,
    AreaService,
    CompanyService,
    UserService,
    RolService,
    QualificationScaleService,
    AppointmentService,
    BehaviourListService,
    CategoryService,
    UploadService,
    CargoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
