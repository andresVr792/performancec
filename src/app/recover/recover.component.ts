import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-recover',
  templateUrl: './recover.component.html',
  styleUrls: ['./recover.component.css']
})
export class RecoverComponent implements OnInit {
  recoverParams:any = {};
  constructor(private router: Router,private authService: AuthService) {
  }
  ngOnInit() {
  }
  recover(){
    this.authService.recover(this.recoverParams);
  }
}
