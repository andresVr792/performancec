import { Component,OnDestroy} from '@angular/core';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'ma-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnDestroy {
  ROLE_ADMIN:boolean ;
  ROLE_RRHH:boolean ;
  ROLE_USER:boolean ;
  user:any={};
  rol:string;
  url:string;

  constructor(private authService:AuthService,
    private userService:UserService){

    this.userService.getUser(localStorage.getItem('uid'))
    .subscribe((user)=>{
      this.user = user;
      localStorage.setItem('rol',this.user.rol);
      localStorage.setItem('company',this.user.company);
      this.rol=localStorage.getItem('rol');
      this.url=localStorage.getItem('imgURL')
      this.validateRole();
    });

  }
  logout(){
    this.authService.logout();
  }
  validateRole(){
		if(this.rol=='ROLE_ADMIN'){
			this.ROLE_ADMIN=true;
			this.ROLE_RRHH=false;
			this.ROLE_USER=false;
		}else if(this.rol=='ROLE_RRHH'){
			this.ROLE_RRHH=true;
			this.ROLE_ADMIN=false;
			this.ROLE_USER=false;
		}else if(this.rol=='ROLE_USER'){
			this.ROLE_USER=true;
			this.ROLE_RRHH=false;
			this.ROLE_ADMIN=false;
		}else{
			this.ROLE_USER=true;
			this.ROLE_RRHH=false;
			this.ROLE_ADMIN=false;
		}
    }
    ngOnDestroy() {
  }

}
