import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'ma-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent implements OnDestroy{
  loggedUser:any = {};
  user:any = {};
  subscription;
  constructor(
    private authService:AuthService,
    private userService:UserService ){
      this.loggedUser = this.authService.getUser();
      this.subscription = this.userService.getUser(this.loggedUser.currentUser.uid)
      .subscribe((user)=>{
        this.user = user;
      });
  }
	logout(){
    this.authService.logout();
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
}
}
