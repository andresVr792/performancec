import { Component, AfterViewInit } from '@angular/core';
import {AppointmentService} from '../../../../services/appointments.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'edit-appointment',
	templateUrl: './edit.component.html'
})
export class EditComponent {
	title:string;
	subtitle:string;
  appointments:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private appointmentService:AppointmentService) {
		this.title = "Editar Competencias";
		this.subtitle = "Edite la información de la competencia";
		this.id = this.route.snapshot.params['id'];
		this.appointmentService.get(this.id)
		.subscribe((appointments)=>{
			this.appointments = appointments;
		});
	}
	edit(){
		this.appointments.editAt = Date.now();
		this.appointmentService.edit(this.appointments);
		this.router.navigate(['appointment']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}
  viewDetail(){
    localStorage.setItem('appointmentID', this.appointments.id);
    localStorage.setItem('appointmentName', this.appointments.name);
    this.router.navigate(['behaviourList']);
  }
}
