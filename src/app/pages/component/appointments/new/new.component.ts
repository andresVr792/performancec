import { Component, AfterViewInit } from '@angular/core';
import { AppointmentService } from '../../../../services/appointments.service';
import { CategoryService } from '../../../../services/category.service';
import { BehaviourListService } from '../../../../services/behaviourList.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'new-appointment',
	templateUrl: './new.component.html'
})
export class NewComponent {
	title:string;
	subtitle:string;
  appointment:any = {};
  categorys:any= {};


	constructor(private router: Router,private appointmentService:AppointmentService,private categoryService:CategoryService,private behaviourListService: BehaviourListService) {
		this.title = "Nueva Competencia";
		this.subtitle = "Ingrese los datos de la competencia";
			}
	save(){
    this.appointment.id = Date.now();
    this.appointment.createAt = Date.now();
    this.appointment.userCreate = '';
    this.appointment.companyId = localStorage.getItem('company');
     this.appointment.state = true;
    this.appointmentService.save(this.appointment);
    localStorage.setItem('appointmentID', this.appointment.id);
    localStorage.setItem('appointmentName', this.appointment.name);
    this.router.navigate(['behaviourList']);
	}


	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
