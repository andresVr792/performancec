import { Component, AfterViewInit } from '@angular/core';
import { AppointmentService } from '../../../../services/appointments.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'view-appointments',
	templateUrl: './view.component.html'
})
export class ViewComponent {
	title:string;
	subtitle:string;
  appointments:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute,private appointmentsService:AppointmentService) {
		this.title = "Información Competencias";
		this.subtitle = "Detalle de su Competenca";
		this.id = this.route.snapshot.params['id'];
		this.appointmentsService.get(this.id)
		.subscribe((appointments)=>{
			this.appointments = appointments;
		});
	}
	changeRoute(routeValue) {
   this.router.navigate([routeValue]);
}
viewDetail(){
  localStorage.setItem('appointmentID', this.appointments.id);
  localStorage.setItem('appointmentName', this.appointments.name);
  this.router.navigate(['behaviourList']);
}

}
