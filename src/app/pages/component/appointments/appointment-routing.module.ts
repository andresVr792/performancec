import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { AppointmentComponent } from './appointment.component';

const appointmentRoutes: Routes = [
  {
    path: 'appointment',
    data: {
        title: 'Appointment',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Competencias'}]
      },
    component: AppointmentComponent,
        children: [
          { path: '', redirectTo: 'list' , pathMatch: 'full',
          data: {
              title: 'Competencias',
              urls: [{title: 'Inicio', url: '/'},{title: 'Competencias', url: '/appointment'},{title: 'Lista'}]
            },
          },
          { path: 'list', component: ListComponent,
          data: {
              title: 'Competencias',
              urls: [{title: 'Inicio', url: '/'},{title: 'Competencias', url: '/appointment'},{title: 'Lista'}]
            },
        },
          { path: 'new', component: NewComponent,
          data: {
              title: 'Competencias',
              urls: [{title: 'Inicio', url: '/'},{title: 'Competencias', url: '/appointment'},{title: 'Nuevo'}]
            },
          },
          { path: 'edit/:id', component: EditComponent,
          data: {
              title: 'Competencias',
              urls: [{title: 'Inicio', url: '/'},{title: 'Competencias'},{title: 'Editar'}]
            },
          },
          { path: 'view/:id', component: ViewComponent,
          data: {
              title: 'Competencias',
              urls: [{title: 'Inicio', url: '/'},{title: 'Competencias'},{title: 'Ver'}]
            },
          },
          { path: 'remove/:id', component: RemoveComponent,
          data: {
              title: 'Competencias',
              urls: [{title: 'Inicio', url: '/'},{title: 'Competencias'},{title: 'Eliminar'}]
            },
          }
        ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(appointmentRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class AppointmentRoutingModule { }
