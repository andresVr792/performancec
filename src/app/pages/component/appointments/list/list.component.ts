import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { AppointmentService } from '../../../../services/appointments.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'list-appointment',
	templateUrl: './list.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
    .dark-modal .modal-content {
      background-color: #009efb;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
  `]
})

export class ListComponent {
  closeResult: string;
	title:string;
	subtitle:string;
  appointments = null;
  appointment =null;
	id:any = null;
  constructor(private router: Router,private route: ActivatedRoute,private appointmentService:AppointmentService, private modalService: NgbModal, private modalService2: NgbModal) {
		this.title = "Competencias";
		this.subtitle = "Todos las Competencias.";
		appointmentService.gets()
		.subscribe(appointment =>{
			this.appointments = appointment;
		});
	}
	remove(id){
		this.appointment.id=id;
		this.appointment.editAt = Date.now();
		this.appointment.state =false;
		this.appointmentService.edit(this.appointment);
		this.router.navigate(['appointment/new']);

	}

  open2(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open(content) {
    this.modalService2.open(content, { windowClass: 'dark-modal' });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
