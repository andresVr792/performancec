import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentComponent } from './appointment.component';
import { HttpModule } from '@angular/http';

import { AppointmentRoutingModule } from './appointment-routing.module';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
	declarations: [
			AppointmentComponent,
			EditComponent,
			NewComponent,
			ViewComponent,
			ListComponent,
			RemoveComponent
	],
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
			AppointmentRoutingModule,
      NgbModule.forRoot()
    ],
		exports: [
			AppointmentComponent,
			EditComponent,
			NewComponent,
			ViewComponent,
			ListComponent,
			RemoveComponent
		],
		providers:[]

})
export class AppointmentModule { }
