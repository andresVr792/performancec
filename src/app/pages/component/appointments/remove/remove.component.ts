import { Component, AfterViewInit } from '@angular/core';
import { AppointmentService } from '../../../../services/appointments.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'remove-appointment',
	templateUrl: './remove.component.html'
})
export class RemoveComponent {
	title:string;
	subtitle:string;
  appointment:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private appointmentService:AppointmentService) {
		this.title = "Eliminar Competencia";
		this.subtitle = "Competencia";
		this.id = this.route.snapshot.params['id'];
		this.appointmentService.get(this.id)
		.subscribe((appointment)=>{
			this.appointment = appointment;
		});
	}
	remove(){
		this.appointment.editAt = Date.now();
		this.appointment.state =false;
		this.appointmentService.edit(this.appointment);
		this.router.navigate(['appointment']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
