import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from '../../../../services/category.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'list-category',
	templateUrl: './list.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
    .dark-modal .modal-content {
      background-color: #009efb;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
  `]
})

export class ListComponent {
  closeResult: string;
	title:string;
	subtitle:string;
	categorys = null;
	category =null;
	id:any = null;
  constructor(private router: Router,private route: ActivatedRoute,private categoryService:CategoryService, private modalService: NgbModal, private modalService2: NgbModal) {
		this.title = "Categorías";
		this.subtitle = "Todos las Categorias.";
		categoryService.gets()
		.subscribe(category =>{
			this.categorys = category;
		});
	}
	remove(id){
		this.category.id=id;
		this.category.editAt = Date.now();
		this.category.state =false;
		this.categoryService.edit(this.category);
		this.router.navigate(['category']);

	}

  open2(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open(content) {
    this.modalService2.open(content, { windowClass: 'dark-modal' });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
