import { Component, AfterViewInit } from '@angular/core';
import { CategoryService } from '../../../../services/category.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'view-category',
	templateUrl: './view.component.html'
})
export class ViewComponent {
	title:string;
	subtitle:string;
  category:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute,private categoryService:CategoryService) {
		this.title = "Información Categoría";
		this.subtitle = "Detalle de su Categoría";
		this.id = this.route.snapshot.params['id'];
		this.categoryService.get(this.id)
		.subscribe((category)=>{
			this.category = category;
		});
	}
	changeRoute(routeValue) {
   this.router.navigate([routeValue]);
}

}
