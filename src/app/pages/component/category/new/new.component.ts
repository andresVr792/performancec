import { Component, AfterViewInit } from '@angular/core';
import { CategoryService } from '../../../../services/category.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'new-category',
	templateUrl: './new.component.html'
})
export class NewComponent {
	title:string;
	subtitle:string;
  category:any = {};

	constructor(private router: Router,private categoryService:CategoryService) {
		this.title = "Nueva Categoría";
		this.subtitle = "Ingrese los datos de la categoría";
	}
	save(){
		this.category.id = Date.now();
		this.category.createAt = Date.now();
		this.category.state=true;
		this.category.userCreate="";
		this.category.companyId = localStorage.getItem('company');
		this.categoryService.save(this.category);
		this.router.navigate(['category']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
