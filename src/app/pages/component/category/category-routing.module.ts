import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { CategoryComponent } from './category.component';

const categoryRoutes: Routes = [
  {
    path: 'category',
    data: {
        title: 'Category',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Categoria'}]
      },
    component: CategoryComponent,
        children: [
          { path: '', redirectTo: 'list' , pathMatch: 'full',
          data: {
              title: 'Categoría',
              urls: [{title: 'Inicio', url: '/'},{title: 'Categoría', url: '/category'},{title: 'Lista'}]
            },
          },
          { path: 'list', component: ListComponent,
          data: {
              title: 'Categoría',
              urls: [{title: 'Inicio', url: '/'},{title: 'Categoría', url: '/category'},{title: 'Lista'}]
            },
        },
          { path: 'new', component: NewComponent,
          data: {
              title: 'Categoría',
              urls: [{title: 'Inicio', url: '/'},{title: 'Categoría', url: '/category'},{title: 'Nuevo'}]
            },
          },
          { path: 'edit/:id', component: EditComponent,
          data: {
              title: 'Categoría',
              urls: [{title: 'Inicio', url: '/'},{title: 'Categoría'},{title: 'Editar'}]
            },
          },
          { path: 'view/:id', component: ViewComponent,
          data: {
              title: 'Categoría',
              urls: [{title: 'Inicio', url: '/'},{title: 'Categoría'},{title: 'Ver'}]
            },
          },
          { path: 'remove/:id', component: RemoveComponent,
          data: {
              title: 'Categoría',
              urls: [{title: 'Inicio', url: '/'},{title: 'Categoría'},{title: 'Eliminar'}]
            },
          }
        ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(categoryRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class CategoryRoutingModule { }
