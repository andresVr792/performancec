import { Component, AfterViewInit } from '@angular/core';
import { CategoryService } from '../../../../services/category.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'remove-category',
	templateUrl: './remove.component.html'
})
export class RemoveComponent {
	title:string;
	subtitle:string;
  category:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private categoryService:CategoryService) {
		this.title = "Eliminar Rol";
		this.subtitle = "Eliminar";
		this.id = this.route.snapshot.params['id'];
		this.categoryService.get(this.id)
		.subscribe((category)=>{
			this.category = category;
		});
	}
	remove(){
		this.category.editAt = Date.now();
		this.category.state =false;
		this.categoryService.edit(this.category);
		this.router.navigate(['category']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
