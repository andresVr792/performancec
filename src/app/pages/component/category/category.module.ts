import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './category.component';
import { HttpModule } from '@angular/http';

import { CategoryRoutingModule } from './category-routing.module';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
	declarations: [
			CategoryComponent,
			EditComponent,
			NewComponent,
			ViewComponent,
			ListComponent,
			RemoveComponent
	],
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
			CategoryRoutingModule,
      NgbModule.forRoot()
    ],
		exports:[
			CategoryComponent,
			EditComponent,
			NewComponent,
			ViewComponent,
			ListComponent,
			RemoveComponent
		],
		providers:[]

})
export class CategoryModule { }
