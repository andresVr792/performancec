import { Component, AfterViewInit } from '@angular/core';
import { CategoryService } from '../../../../services/category.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'edit-category',
	templateUrl: './edit.component.html'
})
export class EditComponent {
	title:string;
	subtitle:string;
  category:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private categoryService:CategoryService) {
		this.title = "Editar Categoría";
		this.subtitle = "Edite la información de la categoría";
		this.id = this.route.snapshot.params['id'];
		this.categoryService.get(this.id)
		.subscribe((category)=>{
			this.category = category;
		});
	}
	edit(){
		this.category.editAt = Date.now();
		this.categoryService.edit(this.category);
		this.router.navigate(['category']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
