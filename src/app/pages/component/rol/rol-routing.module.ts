import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { RolComponent } from './rol.component';

const rolRoutes: Routes = [
  {
    path: 'rol',
    data: {
        title: 'Rol',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Rol'}]
      },
    component: RolComponent,
        children: [
          { path: '', redirectTo: 'list' , pathMatch: 'full',
          data: {
              title: 'Rol',
              urls: [{title: 'Inicio', url: '/'},{title: 'Rol', url: '/rol'},{title: 'Lista'}]
            },
          },
          { path: 'list', component: ListComponent,
          data: {
              title: 'Rol',
              urls: [{title: 'Inicio', url: '/'},{title: 'Rol', url: '/rol'},{title: 'Lista'}]
            },
        },
          { path: 'new', component: NewComponent,
          data: {
              title: 'Rol',
              urls: [{title: 'Inicio', url: '/'},{title: 'Rol', url: '/rol'},{title: 'Nuevo'}]
            },
          },
          { path: 'edit/:id', component: EditComponent,
          data: {
              title: 'Rol',
              urls: [{title: 'Inicio', url: '/'},{title: 'Rol'},{title: 'Editar'}]
            },
          },
          { path: 'view/:id', component: ViewComponent,
          data: {
              title: 'Rol',
              urls: [{title: 'Inicio', url: '/'},{title: 'Rol'},{title: 'Ver'}]
            },
          },
          { path: 'remove/:id', component: RemoveComponent,
          data: {
              title: 'Rol',
              urls: [{title: 'Inicio', url: '/'},{title: 'Rol'},{title: 'Eliminar'}]
            },
          }
        ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(rolRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class RolRoutingModule { }
