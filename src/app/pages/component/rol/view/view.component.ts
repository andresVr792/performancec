import { Component, AfterViewInit } from '@angular/core';
import { RolService } from '../../../../services/rol.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'view-rol',
	templateUrl: './view.component.html'
})
export class ViewComponent {
	title:string;
	subtitle:string;
  rol:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute,private rolService:RolService) {
		this.title = "Información Rol";
		this.subtitle = "Detalle de su rol";
		this.id = this.route.snapshot.params['id'];
		this.rolService.get(this.id)
		.subscribe((rol)=>{
			this.rol = rol;
		});
	}
	changeRoute(routeValue) {
   this.router.navigate([routeValue]);
}

}
