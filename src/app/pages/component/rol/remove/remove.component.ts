import { Component, AfterViewInit } from '@angular/core';
import { RolService } from '../../../../services/rol.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'remove-rol',
	templateUrl: './remove.component.html'
})
export class RemoveComponent {
	title:string;
	subtitle:string;
  rol:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private rolService:RolService) {
		this.title = "Eliminar Rol";
		this.subtitle = "Eliminar";
		this.id = this.route.snapshot.params['id'];
		this.rolService.get(this.id)
		.subscribe((rol)=>{
			this.rol = rol;
		});
	}
	remove(){
		this.rol.editAt = Date.now();
		this.rol.state =false;
		this.rolService.edit(this.rol);
		this.router.navigate(['rol']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
