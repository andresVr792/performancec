import { Component, AfterViewInit } from '@angular/core';
import { RolService } from '../../../../services/rol.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'new-rol',
	templateUrl: './new.component.html'
})
export class NewComponent {
	title:string;
	subtitle:string;
  rol:any = {};

	constructor(private router: Router,private rolService:RolService) {
		this.title = "Nuevo Rol";
		this.subtitle = "Ingrese los datos del Rol";
	}
	save(){
		this.rol.id = Date.now();
		this.rol.createAt = Date.now();
		this.rol.state=true;
		this.rol.userCreate="";
		this.rolService.save(this.rol);
		this.router.navigate(['rol']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
