import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { RolService } from '../../../../services/rol.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'list-rol',
	templateUrl: './list.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
    .dark-modal .modal-content {
      background-color: #009efb;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
  `]
})

export class ListComponent {
  closeResult: string;
	title:string;
	subtitle:string;
	rols = null;
	rol =null;
	id:any = null;
  constructor(private router: Router,private route: ActivatedRoute,private rolService:RolService, private modalService: NgbModal, private modalService2: NgbModal) {
		this.title = "Rol";
		this.subtitle = "Todos los Roles.";
		rolService.gets()
		.subscribe(rol =>{
			this.rols = rol;
		});
	}
	remove(id){
		this.rol.id=id;
		this.rol.editAt = Date.now();
		this.rol.state =false;
		this.rolService.edit(this.rol);
		this.router.navigate(['rol']);

	}

  open2(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open(content) {
    this.modalService2.open(content, { windowClass: 'dark-modal' });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
