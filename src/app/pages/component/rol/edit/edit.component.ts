import { Component, AfterViewInit } from '@angular/core';
import { RolService } from '../../../../services/rol.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'edit-rol',
	templateUrl: './edit.component.html'
})
export class EditComponent {
	title:string;
	subtitle:string;
  rol:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private rolService:RolService) {
		this.title = "Editar Rol";
		this.subtitle = "Edite la información de su rol";
		this.id = this.route.snapshot.params['id'];
		this.rolService.get(this.id)
		.subscribe((rol)=>{
			this.rol = rol;
		});
	}
	edit(){
		this.rol.editAt = Date.now();
		this.rolService.edit(this.rol);
		this.router.navigate(['rol']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
