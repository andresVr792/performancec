import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { GuardService } from '../../../services/guard.service';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { CargoComponent } from './cargo.component';

const cargoRoutes: Routes = [
  {
    path: 'cargo',
    data: {
        title: 'Cargo',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Cargo'}]
      },
    component: CargoComponent,
    canActivate: [GuardService],
    children: [
      { path: '', redirectTo: 'list' , pathMatch: 'full',
      data: {
          title: 'Cargo',
          urls: [{title: 'Inicio', url: '/'},{title: 'Cargo', url: '/cargo'},{title: 'Lista'}]
        },
      },
      { path: 'list', component: ListComponent,
      data: {
          title: 'Cargo',
          urls: [{title: 'Inicio', url: '/'},{title: 'Cargo', url: '/cargo'},{title: 'Lista'}]
        },
    },
      { path: 'new', component: NewComponent,
      data: {
          title: 'Cargo',
          urls: [{title: 'Inicio', url: '/'},{title: 'Cargo', url: '/cargo'},{title: 'Nuevo'}]
        },
      },
      { path: 'edit/:id', component: EditComponent,
      data: {
          title: 'Cargo',
          urls: [{title: 'Inicio', url: '/'},{title: 'Cargo'},{title: 'Editar'}]
        },
      },
      { path: 'view/:id', component: ViewComponent,
      data: {
          title: 'Cargo',
          urls: [{title: 'Inicio', url: '/'},{title: 'Cargo'},{title: 'Ver'}]
        },
      },
      { path: 'remove/:id', component: RemoveComponent,
      data: {
          title: 'Cargo',
          urls: [{title: 'Inicio', url: '/'},{title: 'Cargo'},{title: 'Eliminar'}]
        },
      }
    ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(cargoRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class CargoRoutingModule { }
