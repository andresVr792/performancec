import { Component, Input,OnInit } from '@angular/core';
import { FormsModule,ReactiveFormsModule,Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';

@Component({
    selector: 'tecnical',
    templateUrl: './tecnical.component.html',
})
export class TecnicalComponent implements OnInit{
    @Input('group')
    public tecnicalForm: FormGroup=null;

  	public myForm: FormGroup; // our form model

    constructor(private _fb: FormBuilder) {
    }
    ngOnInit() {
      // we will initialize our form here
      this.myForm = this._fb.group({
  						name: ['', [Validators.required, Validators.minLength(5)]],
  						level: ['', [Validators.required, Validators.minLength(1)]],
  						appointments: ['', [Validators.required, Validators.minLength(1)]],
              evaluators: ['', [Validators.required, Validators.minLength(1)]],
  						tecnicals: this._fb.array([]),
  						standards: this._fb.array([])
          });
    				//add new
  				this.addStandards();
      }
      initStandards() {
            // initialize our address
            return this._fb.group({
              description: ['']
            });
        }
    		addStandards() {
        // add address to the list
        const control = <FormArray>this.myForm.controls['standards'];
    		control.push(this.initStandards());
    		}

    		removeStandards(i: number) {
        // remove address from the list
        const control = <FormArray>this.myForm.controls['standards'];
        control.removeAt(i);
    		}

}
