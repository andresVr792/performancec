export interface Cargo {
    name: string; // required field with minimum 5 characters
		companyId:any;
		level:number;
		appointmentsList:any[];
		evaluatorList:any[];
    tecnicals: TecnicalAppointment[]; // user can have one or more addresses
}

export interface TecnicalAppointment {
    name: string;  // required field
    description: string;
}
