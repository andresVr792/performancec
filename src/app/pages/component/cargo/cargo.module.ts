import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CargoComponent } from './cargo.component';
import { HttpModule } from '@angular/http';

import { CargoRoutingModule } from './cargo-routing.module';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { TecnicalComponent } from './tecnical/tecnical.component';
import { StandardComponent } from './standard/standard.component';

import { GuardService } from '../../../services/guard.service';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
	imports: [
    	FormsModule,
			MultiselectDropdownModule,
    	CommonModule,
      ReactiveFormsModule,
			CargoRoutingModule,
      NgbModule.forRoot()
    ],
	declarations: [
			CargoComponent,
			EditComponent,
			NewComponent,
			ViewComponent,
			ListComponent,
			RemoveComponent,
			TecnicalComponent,
			StandardComponent
	],
	providers:[
		GuardService
	]
})
export class CargoModule { }
