import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'standard',
    templateUrl: './standard.component.html',
})
export class StandardComponent {
    @Input('group')
    public standardForm: FormGroup=null;
}
