import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { CargoService } from '../../../../services/cargo.service'
import { AppointmentService } from '../../../../services/appointments.service'
import { UserService } from '../../../../services/user.service'
import { CompanyService } from '../../../../services/company.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'list-cargo',
	templateUrl: './list.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
    .dark-modal .modal-content {
      background-color: #009efb;
      color: white;
    }
    .dark-modal .close {
      color: white;s
    }
  `]
})

export class ListComponent {
  closeResult: string;
	title:string;
	subtitle:string;
	cargos = null;
	users = null;
	appointments = null;
	id:any = null;
  constructor(
		private router: Router,
		private route: ActivatedRoute,
		private cargoService:CargoService,
		private userService:UserService,
		private appointmentsService:AppointmentService,
		private companyService:CompanyService) {
		this.title = "Cargos";
		this.subtitle = "Todos los Cargos.";
		cargoService.gets()
		.subscribe(cargo =>{
			this.cargos = cargo;
		});
		this.userService.getUsers()
		.subscribe((users)=>{
			this.users=users
		});
		this.appointmentsService.gets()
		.subscribe((appointment)=>{
			this.appointments=appointment
		});


	}

}
