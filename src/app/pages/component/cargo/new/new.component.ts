import { Component, AfterViewInit,OnInit } from '@angular/core';
import { CargoService } from '../../../../services/cargo.service';
import { AppointmentService } from '../../../../services/appointments.service';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute,Router } from '@angular/router';
import { FormsModule,ReactiveFormsModule,Validators, FormGroup, FormArray, FormBuilder  } from '@angular/forms';
import { Cargo } from '../cargo.interface';
import { MultiSelectSearchFilter } from './search-filter.pipe';
import { IMultiSelectOption,IMultiSelectOptionUser, IMultiSelectSettings, IMultiSelectTexts } from './types';

@Component({
	selector: 'new-cargo',
	templateUrl: './new.component.html'
})

export class NewComponent implements OnInit {
	title:string;
	subtitle:string;
  cargo:any = {};
	optionsModel: number[];
	Appointments: IMultiSelectOption[];
  Users: IMultiSelectOption[];
	// Settings configuration
	mySettings: IMultiSelectSettings = {
	    enableSearch: true,
	    checkedStyle: 'fontawesome',
	    buttonClasses: 'btn btn-primary btn-block  btn-md',
	    dynamicTitleMaxItems: 4,
	    displayAllSelectedText: true
	};

	// Text configuration
	myTexts: IMultiSelectTexts = {
	    checkAll: 'Selecionar Todos',
	    uncheckAll: 'Deselecionar',
	    checked: 'item seleccionado',
	    checkedPlural: 'items selecionandos',
	    searchPlaceholder: 'Buscar',
	    searchEmptyResult: 'Nada encontrado...',
	    searchNoRenderText: 'Type in search box to see results...',
	    defaultTitle: 'Selecionar',
	    allSelected: 'Todos Selecionados',
	};
	public myForm: FormGroup; // our form model

	    // we will use form builder to simplify our syntax
	constructor(private _fb: FormBuilder,
		private router: Router,
		private cargoService:CargoService,
		private appointmentsService:AppointmentService,
		private userService:UserService) {
		this.title = "Nuevo Cargo";
		this.appointmentsService.gets()
		.subscribe((appointments)=>{
			this.Appointments=appointments
		})
		this.userService.getUsers()
		.subscribe((users)=>{
			this.Users=users
		})
	}
	ngOnInit() {
    // we will initialize our form here
    this.myForm = this._fb.group({
						name: ['', [Validators.required, Validators.minLength(5)]],
						level: ['', [Validators.required, Validators.minLength(1)]],
						appointments: ['', [Validators.required, Validators.minLength(1)]],
            evaluators: ['', [Validators.required, Validators.minLength(1)]],
						tecnicals: this._fb.array([])
        });
				//add new
				this.addTecnicals();
    }
	save(myForm: FormGroup){
		this.cargo.id = Date.now();
		this.cargo.createAt = Date.now();
		this.cargo.tecnicalApponintmentsList = myForm.controls.tecnicals.value;
		this.cargo.companyId = localStorage.getItem('company')
		this.cargo.userCreate= localStorage.getItem('uid')
		this.cargoService.save(this.cargo);
		this.router.navigate(['cargo']);
	}
	initTecnicals() {
        // initialize our address
        return this._fb.group({
            funcion: ['', Validators.required]
        });
    }
		addTecnicals() {
    // add address to the list
    const control = <FormArray>this.myForm.controls['tecnicals'];
		control.push(this.initTecnicals());
		}

		removeTecnicals(i: number) {
    // remove address from the list
    const control = <FormArray>this.myForm.controls['tecnicals'];
    control.removeAt(i);
		}
	onChange() {
		//console.log(this.cargo.appointmentsList);
    }
	onChangeu() {
	  //console.log(this.cargo.evaluatorList);
	  }
}
