import { Component, AfterViewInit,OnInit } from '@angular/core';
import { QualificationScaleService } from '../../../../services/qualificationScale.service';
import { ActivatedRoute,Router } from '@angular/router';
import { FormsModule,ReactiveFormsModule,Validators, FormGroup, FormArray, FormBuilder  } from '@angular/forms';
import { QScale } from '../qScale.interface';
import { MultiSelectSearchFilter } from './search-filter.pipe';
import { IMultiSelectOption,IMultiSelectOptionUser, IMultiSelectSettings, IMultiSelectTexts } from './types';

@Component({
	selector: 'new-qScale',
	templateUrl: './new.component.html'
})

export class NewComponent implements OnInit {
	title:string;
	subtitle:string;
  qScale:any = {};
	optionsModel: number[];
	// Settings configuration
	mySettings: IMultiSelectSettings = {
	    enableSearch: true,
	    checkedStyle: 'fontawesome',
	    buttonClasses: 'btn btn-primary btn-block  btn-md',
	    dynamicTitleMaxItems: 4,
	    displayAllSelectedText: true
	};

	// Text configuration
	myTexts: IMultiSelectTexts = {
	    checkAll: 'Selecionar Todos',
	    uncheckAll: 'Deselecionar',
	    checked: 'item seleccionado',
	    checkedPlural: 'items selecionandos',
	    searchPlaceholder: 'Buscar',
	    searchEmptyResult: 'Nada encontrado...',
	    searchNoRenderText: 'Type in search box to see results...',
	    defaultTitle: 'Selecionar',
	    allSelected: 'Todos Selecionados',
	};
	public myForm: FormGroup; // our form model

	    // we will use form builder to simplify our syntax
	constructor(private _fb: FormBuilder,
		private router: Router,
		private qualificationScaleService:QualificationScaleService,) {
		this.title = "Nueva Escala";
		this.qualificationScaleService.gets()
		.subscribe((qScale)=>{
			this.qScale=qScale
		})

	}
	ngOnInit() {
    // we will initialize our form here
    this.myForm = this._fb.group({
						name: ['', [Validators.required, Validators.minLength(5)]],
					  tecnicals: this._fb.array([])
        });
				//add new
				this.addTecnicals();
    }
	save(myForm: FormGroup){
		this.qScale.id = Date.now();
		this.qScale.createAt = Date.now();
		this.qScale.qualification = myForm.controls.tecnicals.value;
		this.qScale.companyId = localStorage.getItem('company')
		this.qScale.userCreate= localStorage.getItem('uid')
    this.qualificationScaleService.save(this.qScale);
		this.router.navigate(['cargo']);
	}
	initTecnicals() {
        // initialize our address
        return this._fb.group({
            name: ['', Validators.required],
            value: ['', Validators.required],
            detail: ['']
        });
    }
		addTecnicals() {
    // add address to the list
    const control = <FormArray>this.myForm.controls['tecnicals'];
		control.push(this.initTecnicals());
		}

		removeTecnicals(i: number) {
    // remove address from the list
    const control = <FormArray>this.myForm.controls['tecnicals'];
    control.removeAt(i);
		}
	onChange() {
		//console.log(this.cargo.appointmentsList);
    }
	onChangeu() {
	  //console.log(this.cargo.evaluatorList);
	  }
}
