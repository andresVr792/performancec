import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { QualificationScaleService } from '../../../../services/qualificationScale.service'
import { CompanyService } from '../../../../services/company.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'list-qScale',
	templateUrl: './list.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
    .dark-modal .modal-content {
      background-color: #009efb;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
  `]
})

export class ListComponent {
  closeResult: string;
	title:string;
	subtitle:string;
  qScales = null;
	id:any = null;
  constructor(
		private router: Router,
		private route: ActivatedRoute,
		private qualificationScaleService: QualificationScaleService,
		private companyService:CompanyService) {
		this.title = "Escalas de Calificación";
		this.subtitle = "Todas las Escalas.";
		qualificationScaleService.gets()
		.subscribe(qScales =>{
			this.qScales = qScales;
		});


	}

}
