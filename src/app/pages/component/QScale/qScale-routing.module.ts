import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { GuardService } from '../../../services/guard.service';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { QScaleComponent } from './qScale.component';

const qScaleRoutes: Routes = [
  {
    path: 'qScale',
    data: {
        title: 'QScale',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Escala de Calificación'}]
      },
    component: QScaleComponent,
    canActivate: [GuardService],
    children: [
      { path: '', redirectTo: 'list' , pathMatch: 'full',
      data: {
          title: 'Escala de Calificación',
          urls: [{title: 'Inicio', url: '/'},{title: 'Escala de Calificación', url: '/qScale'},{title: 'Lista'}]
        },
      },
      { path: 'list', component: ListComponent,
      data: {
          title: 'Escala de Calificación',
          urls: [{title: 'Inicio', url: '/'},{title: 'Escala de Calificación', url: '/qScale'},{title: 'Lista'}]
        },
    },
      { path: 'new', component: NewComponent,
      data: {
          title: 'Escala de Calificación',
          urls: [{title: 'Inicio', url: '/'},{title: 'Escala de Calificación', url: '/qScale'},{title: 'Nuevo'}]
        },
      },
      { path: 'edit/:id', component: EditComponent,
      data: {
          title: 'Escala de Calificación',
          urls: [{title: 'Inicio', url: '/'},{title: 'Escala de Calificación'},{title: 'Editar'}]
        },
      },
      { path: 'view/:id', component: ViewComponent,
      data: {
          title: 'Escala de Calificación',
          urls: [{title: 'Inicio', url: '/'},{title: 'Escala de Calificación'},{title: 'Ver'}]
        },
      },
      { path: 'remove/:id', component: RemoveComponent,
      data: {
          title: 'Escala de Calificación',
          urls: [{title: 'Inicio', url: '/'},{title: 'Escala de Calificación'},{title: 'Eliminar'}]
        },
      }
    ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(qScaleRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class QScaleRoutingModule { }
