import { Component, AfterViewInit } from '@angular/core';
import { QualificationScaleService } from '../../../../services/qualificationScale.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'remove-qScale',
	templateUrl: './remove.component.html'
})
export class RemoveComponent {
	title:string;
	subtitle:string;
  qScale:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private qualificationScaleService: QualificationScaleService) {
		this.title = "Eliminar Escala de Calificación";
		this.subtitle = "Eliminar";
		this.id = this.route.snapshot.params['id'];
		this.qualificationScaleService.get(this.id)
		.subscribe((qScale)=>{
			this.qScale = qScale;
		});
	}
	removeScale(){
	  this.qualificationScaleService.r(this.qScale);
		this.router.navigate(['qScale']);
	}
	changeRoute(routeValue) {

   this.router.navigate([routeValue]);
   // you have to check this out by passing required route value.

}


}
