export interface QScale {
    name: string; // required field with minimum 5 characters
    companyId: any;
    qualification: Scale[]; // user can have one or more addresses
}

export interface Scale {
    text: string;  // required field
    value: number;
    description: string;
}
