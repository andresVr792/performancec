import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { EditCompanyComponent } from './edit/edit.component';
import { NewCompanyComponent } from './new/new.component';
import { ListCompanyComponent } from './list/list.component';
import { ViewCompanyComponent } from './view/view.component';
import { RemoveCompanyComponent } from './remove/remove.component';
import { CompanyComponent } from './company.component';

const companyRoutes: Routes = [
  {
    path: 'company',
    data: {
        title: 'Empresa',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Empresa'}]
      },
    component: CompanyComponent,
    children: [
      {
      path: '',
      redirectTo: 'list' ,
      pathMatch: 'full',
      data: {
          title: 'Empresa',
          urls: [{title: 'Inicio', url: '/'},{title: 'Empresa', url: '/company'},{title: 'Lista'}]
        },
      },
      {
        path: 'list',
        component: ListCompanyComponent,
        data: {
            title: 'Empresa',
            urls: [{title: 'Inicio', url: '/'},{title: 'Empresa', url: '/company'},{title: 'Lista'}]
          },
    },
      {
        path: 'new',
        component: NewCompanyComponent,
        data: {
            title: 'Empresa',
            urls: [{title: 'Inicio', url: '/'},{title: 'Empresa', url: '/company'},{title: 'Nueva'}]
          },
        },
      {
        path: 'edit/:id',
        component: EditCompanyComponent,
        data: {
            title: 'Empresa',
            urls: [{title: 'Inicio', url: '/'},{title: 'Empresa', url: '/company'},{title: 'Editar'}]
          },
      },
      {
        path: 'view/:id',
        component: ViewCompanyComponent,
        data: {
            title: 'Empresa',
            urls: [{title: 'Inicio', url: '/'},{title: 'Empresa', url: '/company'},{title: 'Ver'}]
          },
      },
      {
        path: 'remove/:id',
        component: RemoveCompanyComponent,
        data: {
            title: 'Empresa',
            urls: [{title: 'Inicio', url: '/'},{title: 'Empresa', url: '/company'},{title: 'Eliminar'}]
          },
      }
    ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(companyRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class CompanyRoutingModule { }
