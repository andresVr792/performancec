import { Component, AfterViewInit } from '@angular/core';
import { CompanyService } from '../../../../services/company.service';
import { ActivatedRoute,Router } from '@angular/router';
import {Upload} from '../../../../model/upload';
import { UploadService } from '../../../../services/upload.service';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import {FormControl} from '@angular/forms';
import {Http} from '@angular/http';

@Component({
	selector: 'edit-company',
	templateUrl: './edit.component.html'
})
export class EditCompanyComponent {
	title:string;
	subtitle:string;
  company:any = {};
	id:any = null;
	results$: Observable<any>;
	private searchField: FormControl;
	file:any = {};
	selectedFiles: FileList;
	currentUpload : Upload;

	constructor(private router: Router,
		private route: ActivatedRoute,
		private companyService:CompanyService,
		private uploadService:UploadService,
		private http:Http) {
		this.title = "Editar Empresa";
		this.subtitle = "Edite la información de su empresa";
		this.id = this.route.snapshot.params['id'];
		this.companyService.getCompany(this.id)
		.subscribe((company)=>{
			this.company = company;
		});
		const URL = 'https://maps.google.com/maps/api/geocode/json';
		this.searchField = new FormControl();
		this.results$ = this.searchField.valueChanges
		.debounceTime(500)
		.switchMap(query => this.http.get(`${URL}?address=${query}`))
		.map(response => response.json())
		.map(response => response.results);
	}
	editCompany(){
		let file = this.selectedFiles.item(0)
		this.company.editAt = Date.now();
		this.company.imgName = file.name;
		this.currentUpload = new Upload(file);
		this.company.state=true;
		this.uploadService.pushUpload(this.currentUpload);
		this.companyService.editCompany(this.company);
		this.router.navigate(['company']);
	}
	detectFiles(event) {
      this.selectedFiles = event.target.files;
  }
	selectAddress(address){
		this.company.street = address.address_components[1].long_name;
		this.company.city = address.address_components[3].long_name;
		this.company.country = address.address_components[6].long_name;
		this.company.zipcode = address.address_components[7].long_name;
		this.company.provincia = address.address_components[5].long_name;
		this.company.number = address.address_components[0].long_name;
	}


}
