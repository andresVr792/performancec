import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CompanyComponent } from './company.component';
import { HttpModule } from '@angular/http';

import { CompanyRoutingModule } from './company-routing.module';
import { EditCompanyComponent } from './edit/edit.component';
import { NewCompanyComponent } from './new/new.component';
import { ListCompanyComponent } from './list/list.component';
import { ViewCompanyComponent } from './view/view.component';
import { RemoveCompanyComponent } from './remove/remove.component';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
			CompanyRoutingModule,
      NgbModule.forRoot()
    ],
	declarations: [
			CompanyComponent,
			EditCompanyComponent,
			NewCompanyComponent,
			ViewCompanyComponent,
			ListCompanyComponent,
			RemoveCompanyComponent
	]
})
export class CompanyModule { }
