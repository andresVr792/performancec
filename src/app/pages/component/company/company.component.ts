import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { CompanyService } from '../../../services/company.service'
@Component({
	selector: 'ngbd-company',
	templateUrl: './company.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
    .dark-modal .modal-content {
      background-color: #009efb;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
  `]
})

export class CompanyComponent {
  closeResult: string;
	title:string;
	subtitle:string;
	companies = null;

  constructor(private companyService:CompanyService, private modalService: NgbModal, private modalService2: NgbModal) {
		this.title = "Empresa";
		this.subtitle = "Todas las empresas.";
		companyService.getCompanies()
		.subscribe(company =>{
			this.companies = company;
		});
	}

  open2(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open(content) {
    this.modalService2.open(content, { windowClass: 'dark-modal' });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
