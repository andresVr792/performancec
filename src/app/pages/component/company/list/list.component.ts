import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { CompanyService } from '../../../../services/company.service'
import { UploadService } from '../../../../services/upload.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'list-company',
	templateUrl: './list.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
    .dark-modal .modal-content {
      background-color: #009efb;
      color: white;
    }
    .dark-modal .close {
      color: white;s
    }
  `]
})

export class ListCompanyComponent {
  closeResult: string;
	title:string;
	subtitle:string;
	companies = null;
	company =null;
	id:any = null;
	url:any = null;
  constructor(private router: Router,
		private route: ActivatedRoute,
		private companyService:CompanyService,
		private uploadService:UploadService,
		private modalService: NgbModal,
		private modalService2: NgbModal) {
		this.title = "Empresa";
		this.subtitle = "Todas las empresas.";
		companyService.getCompanies()
		.subscribe(company =>{
			this.companies = company;
		});
	}
	removeCompany(id){
		this.company.id=id;
		this.company.editAt = Date.now();
		this.company.state =false;
		this.companyService.editCompany(this.company);
		this.router.navigate(['company']);

	}

  open2(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open(content) {
    this.modalService2.open(content, { windowClass: 'dark-modal' });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
