import { Component, AfterViewInit,OnInit } from '@angular/core';
import { CompanyService } from '../../../../services/company.service';
import { UploadService } from '../../../../services/upload.service';
import { ActivatedRoute,Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import {FormControl} from '@angular/forms';
import {Http} from '@angular/http';
import {Upload} from '../../../../model/upload';


@Component({
	selector: 'new-company',
	templateUrl: './new.component.html'
})

export class NewCompanyComponent implements OnInit{
	title:string;
	subtitle:string;
  company:any = {};
	file:any = {};
	results$: Observable<any>;
	private searchField: FormControl;
	selectedFiles: FileList;
	currentUpload : Upload;

	constructor(private router: Router,
		private companyService:CompanyService,
		private uploadService:UploadService,
		private http:Http) {
		this.title = "Nueva Empresa";
		this.subtitle = "Ingrese los datos de su empresa";
		const URL = 'https://maps.google.com/maps/api/geocode/json';
		this.searchField = new FormControl();
		this.results$ = this.searchField.valueChanges
		.debounceTime(800)
		.switchMap(query => this.http.get(`${URL}?address=${query}`))
		.map(response => response.json())
		.map(response => response.results);

	}
	ngOnInit() {

  }
	saveCompany(){
		let file = this.selectedFiles.item(0)
		this.company.id = Date.now();
		this.company.imgName = file.name;
    this.currentUpload = new Upload(file);
		this.company.createAt = Date.now();
		this.company.state=true;
		this.company.companyId = localStorage.getItem('company')
		this.uploadService.pushUpload(this.currentUpload);
		this.companyService.saveCompany(this.company);
		this.router.navigate(['company']);
	}
	detectFiles(event) {
      this.selectedFiles = event.target.files;
  }
	selectAddress(address){
		this.company.street = address.address_components[1].long_name;
		this.company.city = address.address_components[3].long_name;
		this.company.country = address.address_components[6].long_name;
		this.company.zipcode = address.address_components[7].long_name;
		this.company.provincia = address.address_components[5].long_name;
		this.company.number = address.address_components[0].long_name;
	}


}
