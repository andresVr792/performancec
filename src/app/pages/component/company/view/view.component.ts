import { Component, AfterViewInit } from '@angular/core';
import { CompanyService } from '../../../../services/company.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'view-company',
	templateUrl: './view.component.html'
})
export class ViewCompanyComponent {
	title:string;
	subtitle:string;
  company:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute,private companyService:CompanyService) {
		this.title = "Información Empresa";
		this.subtitle = "Detalle de su empresa";
		this.id = this.route.snapshot.params['id'];
		console.log(this.id);
		this.companyService.getCompany(this.id)
		.subscribe((company)=>{
			this.company = company;
		});
	}
	changeRoute(routeValue) {
   //this._LoaderService.show();
   //this will start the loader service.

   this.router.navigate([routeValue]);
   // you have to check this out by passing required route value.
   // this line will redirect you to your destination. By reaching to destination you can close your loader service.
   // please note this implementation may vary according to your routing code.

}

}
