import { Component, AfterViewInit } from '@angular/core';
import { CompanyService } from '../../../../services/company.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'remove-company',
	templateUrl: './remove.component.html'
})
export class RemoveCompanyComponent {
	title:string;
	subtitle:string;
  company:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private companyService:CompanyService) {
		this.title = "Eliminar Empresa";
		this.subtitle = "Eliminar";
		this.id = this.route.snapshot.params['id'];
		this.companyService.getCompany(this.id)
		.subscribe((company)=>{
			this.company = company;
		});
	}
	removeCompany(){
		this.company.editAt = Date.now();
		this.company.state =false;
		this.companyService.editCompany(this.company);
		this.router.navigate(['company']);
	}


}
