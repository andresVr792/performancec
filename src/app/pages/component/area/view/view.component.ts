import { Component, AfterViewInit } from '@angular/core';
import { CargoService } from '../../../../services/cargo.service';
import { AreaService } from '../../../../services/area.service';
import { ActivatedRoute,Router } from '@angular/router';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from '../new/types';

@Component({
	selector: 'view-area',
	templateUrl: './view.component.html'
})
export class ViewComponent {
	title:string;
	subtitle:string;
  area:any = {};
  Cargos: IMultiSelectOption[];
	id:any = null;

  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-primary btn-block  btn-lg',
    dynamicTitleMaxItems: 100,
    displayAllSelectedText: true
  };
  myTexts: IMultiSelectTexts = {
    checkAll: 'Selecionar Todos',
    uncheckAll: 'Deselecionar',
    checked: 'item seleccionado',
    checkedPlural: 'items selecionandos',
    searchPlaceholder: 'Buscar',
    searchEmptyResult: 'Nada encontrado...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Selecionar',
    allSelected: 'Todos Selecionados',
  };

	constructor(private router: Router,private route: ActivatedRoute,private cargoService:CargoService,private areaService:AreaService) {
		this.title = "Información del Área";
		this.subtitle = "Detalle de su Área";
		this.id = this.route.snapshot.params['id'];
		this.areaService.get(this.id)
		.subscribe((area)=>{
			this.area = area;
		});
    this.cargoService.gets()
      .subscribe((cargo)=>{
        this.Cargos=cargo
      });

  }
	changeRoute(routeValue) {

   this.router.navigate([routeValue]);
   // you have to check this out by passing required route value.

}

}
