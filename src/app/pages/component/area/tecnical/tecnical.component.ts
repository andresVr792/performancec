import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'tecnical',
    templateUrl: './tecnical.component.html',
})
export class TecnicalComponent {
    @Input('group')
    public tecnicalForm: FormGroup=null;
}
