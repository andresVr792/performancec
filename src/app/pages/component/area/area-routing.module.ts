import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { GuardService } from '../../../services/guard.service';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { AreaComponent } from './area.component';

const areaRoutes: Routes = [
  {
    path: 'area',
    data: {
        title: 'Area',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Areas'}]
      },
    component: AreaComponent,
    canActivate: [GuardService],
    children: [
      { path: '', redirectTo: 'list' , pathMatch: 'full',
      data: {
          title: 'Areas',
          urls: [{title: 'Inicio', url: '/'},{title: 'Areas', url: '/area'},{title: 'Lista'}]
        },
      },
      { path: 'list', component: ListComponent,
      data: {
          title: 'Areas',
          urls: [{title: 'Inicio', url: '/'},{title: 'Areas', url: '/area'},{title: 'Lista'}]
        },
    },
      { path: 'new', component: NewComponent,
      data: {
          title: 'Areas',
          urls: [{title: 'Inicio', url: '/'},{title: 'Areas', url: '/area'},{title: 'Nuevo'}]
        },
      },
      { path: 'edit/:id', component: EditComponent,
      data: {
          title: 'Areas',
          urls: [{title: 'Inicio', url: '/'},{title: 'Areas'},{title: 'Editar'}]
        },
      },
      { path: 'view/:id', component: ViewComponent,
      data: {
          title: 'Areas',
          urls: [{title: 'Inicio', url: '/'},{title: 'Areas'},{title: 'Ver'}]
        },
      },
      { path: 'remove/:id', component: RemoveComponent,
      data: {
          title: 'Areas',
          urls: [{title: 'Inicio', url: '/'},{title: 'Areas'},{title: 'Eliminar'}]
        },
      }
    ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(areaRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class AreaRoutingModule { }
