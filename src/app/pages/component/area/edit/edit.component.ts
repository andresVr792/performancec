import { Component, AfterViewInit,OnInit } from '@angular/core';
import { GroupService } from '../../../../services/group.service';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute,Router } from '@angular/router';
import { FormsModule,ReactiveFormsModule,Validators, FormGroup, FormArray, FormBuilder  } from '@angular/forms';
import { Area } from '../area.interface';
import { MultiSelectSearchFilter } from '../new/search-filter.pipe';
import { IMultiSelectOption,IMultiSelectOptionUser, IMultiSelectSettings, IMultiSelectTexts } from '../new/types';

@Component({
	selector: 'edit-area',
	templateUrl: './edit.component.html'
})

export class EditComponent implements OnInit {
	title:string;
	subtitle:string;
  group:any = {};
  Users: IMultiSelectOption[];
	id:any = null;

	// Settings configuration
	mySettings: IMultiSelectSettings = {
	    enableSearch: true,
	    checkedStyle: 'fontawesome',
	    buttonClasses: 'btn btn-primary btn-block  btn-md',
	    dynamicTitleMaxItems: 4,
	    displayAllSelectedText: true
	};

	// Text configuration
	myTexts: IMultiSelectTexts = {
	    checkAll: 'Selecionar Todos',
	    uncheckAll: 'Deselecionar',
	    checked: 'item seleccionado',
	    checkedPlural: 'items selecionandos',
	    searchPlaceholder: 'Buscar',
	    searchEmptyResult: 'Nada encontrado...',
	    searchNoRenderText: 'Type in search box to see results...',
	    defaultTitle: 'Selecionar',
	    allSelected: 'Todos Selecionados',
	};
	public myForm: FormGroup; // our form model

	    // we will use form builder to simplify our syntax
	constructor(private _fb: FormBuilder,
		private router: Router,
		private groupService: GroupService,
		private route: ActivatedRoute,
		private userService:UserService) {
		this.title = "Nuevo Grupo";
		this.id = this.route.snapshot.params['id'];
		this.groupService.get(this.id)
		.subscribe((group)=>{
			this.group = group;
		});
		this.userService.getUsers()
		.subscribe((users)=>{
			this.Users=users
		});
	}
	ngOnInit() {
    // we will initialize our form here
    this.myForm = this._fb.group({
						name: ['', [Validators.required, Validators.minLength(5)]],
            members: ['', [Validators.required, Validators.minLength(1)]],
        });


    }
	edit(myForm: FormGroup){
		this.group.userEdit= localStorage.getItem('uid')
		this.groupService.save(this.group);
		this.router.navigate(['group']);
	}
	initTecnicals() {
        // initialize our address
        return this._fb.group({
            name: ['', Validators.required],
            description: ['']
        });
    }

	onChange() {
		//console.log(this.cargo.appointmentsList);
    }
	onChangeu() {
	  //console.log(this.cargo.evaluatorList);
	  }
}
