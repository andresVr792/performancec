import { Component, AfterViewInit,OnInit } from '@angular/core';
import { AreaService } from '../../../../services/area.service';
import { CargoService } from '../../../../services/cargo.service';
import { ActivatedRoute,Router } from '@angular/router';
import { FormsModule,ReactiveFormsModule,Validators, FormGroup, FormArray, FormBuilder  } from '@angular/forms';
import { Area } from '../area.interface';
import { MultiSelectSearchFilter } from './search-filter.pipe';
import { IMultiSelectOption,IMultiSelectOptionUser, IMultiSelectSettings, IMultiSelectTexts } from './types';

@Component({
	selector: 'new-area',
	templateUrl: './new.component.html'
})

export class NewComponent implements OnInit {
	title:string;
	subtitle:string;
  area:any = {};
  Cargos: IMultiSelectOption[];
	// Settings configuration
	mySettings: IMultiSelectSettings = {
	    enableSearch: true,
	    checkedStyle: 'fontawesome',
	    buttonClasses: 'btn btn-primary btn-block  btn-md',
	    dynamicTitleMaxItems: 4,
	    displayAllSelectedText: true
	};

	// Text configuration
	myTexts: IMultiSelectTexts = {
	    checkAll: 'Selecionar Todos',
	    uncheckAll: 'Deselecionar',
	    checked: 'item seleccionado',
	    checkedPlural: 'items selecionandos',
	    searchPlaceholder: 'Buscar',
	    searchEmptyResult: 'Nada encontrado...',
	    searchNoRenderText: 'Type in search box to see results...',
	    defaultTitle: 'Selecionar',
	    allSelected: 'Todos Selecionados',
	};
	public myForm: FormGroup; // our form model

	    // we will use form builder to simplify our syntax
	constructor(private _fb: FormBuilder,
		private router: Router,
		private cargoService:CargoService,
		private areaService:AreaService) {
		this.title = "Nueva Área";

		this.cargoService.gets()
		.subscribe((cargo)=>{
			this.Cargos=cargo
		})
	}
	ngOnInit() {
    // we will initialize our form here
    this.myForm = this._fb.group({
						name: ['', [Validators.required, Validators.minLength(5)]],
					members: ['', [Validators.required, Validators.minLength(1)]],
           });

    }
	save(myForm: FormGroup){
		this.area.id = Date.now();
		this.area.createAt = Date.now();
		this.area.companyId = localStorage.getItem('company')
		this.area.userCreate= localStorage.getItem('uid')
		this.areaService.save(this.area);
		this.router.navigate(['area']);
	}
	initTecnicals() {
        // initialize our address
        return this._fb.group({
            name: ['', Validators.required],
            description: ['']
        });
    }
  onChange() {
    //console.log(this.cargo.appointmentsList);
  }
  onChangeu() {
    //console.log(this.cargo.evaluatorList);
  }
  changeRoute(routeValue) {
    this.router.navigate([routeValue]);
  }
}
