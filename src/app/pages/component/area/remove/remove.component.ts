import { Component, AfterViewInit } from '@angular/core';
import { AreaService } from '../../../../services/area.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'remove-area',
	templateUrl: './remove.component.html'
})
export class RemoveComponent {
	title:string;
	subtitle:string;
  area:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private areaService: AreaService) {
		this.title = "Eliminar Área";
		this.subtitle = "Eliminar";
		this.id = this.route.snapshot.params['id'];
		this.areaService.get(this.id)
		.subscribe((area)=>{
			this.area = area;
		});
	}
	removeArea(){
		this.areaService.remove(this.area);
		this.router.navigate(['area']);
	}
	changeRoute(routeValue) {

   this.router.navigate([routeValue]);
   // you have to check this out by passing required route value.

}


}
