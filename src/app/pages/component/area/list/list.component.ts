import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { AreaService } from '../../../../services/area.service'
import { CargoService } from '../../../../services/cargo.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from '../new/types';

@Component({
	selector: 'list-area',
	templateUrl: './list.component.html'
})

export class ListComponent {
  closeResult: string;
	title:string;
	subtitle:string;
	areas:any = {};
	Cargos: IMultiSelectOption[];
	id:any = null;
	// Settings configuration
	mySettings: IMultiSelectSettings = {
	    enableSearch: true,
	    checkedStyle: 'fontawesome',
	    buttonClasses: 'btn btn-primary btn-block  btn-lg',
	    dynamicTitleMaxItems: 100,
	    displayAllSelectedText: true
	};

	// Text configuration
	myTexts: IMultiSelectTexts = {
	    checkAll: 'Selecionar Todos',
	    uncheckAll: 'Deselecionar',
	    checked: 'item seleccionado',
	    checkedPlural: 'items selecionandos',
	    searchPlaceholder: 'Buscar',
	    searchEmptyResult: 'Nada encontrado...',
	    searchNoRenderText: 'Type in search box to see results...',
	    defaultTitle: 'Selecionar',
	    allSelected: 'Todos Selecionados',
	};
  constructor(
		private router: Router,
		private route: ActivatedRoute,
		private areaService:AreaService,private cargoService:CargoService) {
		this.title = "Areas";
		this.subtitle = "Todas las areas.";
		this.areaService.gets()
		.subscribe(area =>{
			this.areas = area;
		});
		this.cargoService.gets()
		.subscribe((cargo)=>{
			this.Cargos=cargo
		});



	}
	// getUser(id){
	//
	// 	this.userService.getUser(id)
	// 	.subscribe(user =>{
	// 		this.user = user;
	// 		console.log(this.user)
	// 	});
	//
	// }

}
