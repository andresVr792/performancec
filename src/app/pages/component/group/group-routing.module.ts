import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { GuardService } from '../../../services/guard.service';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { GroupComponent } from './group.component';

const groupRoutes: Routes = [
  {
    path: 'group',
    data: {
        title: 'Group',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Grupos'}]
      },
    component: GroupComponent,
    canActivate: [GuardService],
    children: [
      { path: '', redirectTo: 'list' , pathMatch: 'full',
      data: {
          title: 'Grupos',
          urls: [{title: 'Inicio', url: '/'},{title: 'Grupos', url: '/group'},{title: 'Lista'}]
        },
      },
      { path: 'list', component: ListComponent,
      data: {
          title: 'Grupos',
          urls: [{title: 'Inicio', url: '/'},{title: 'Grupos', url: '/group'},{title: 'Lista'}]
        },
    },
      { path: 'new', component: NewComponent,
      data: {
          title: 'Grupos',
          urls: [{title: 'Inicio', url: '/'},{title: 'Grupos', url: '/group'},{title: 'Nuevo'}]
        },
      },
      { path: 'edit/:id', component: EditComponent,
      data: {
          title: 'Grupos',
          urls: [{title: 'Inicio', url: '/'},{title: 'Grupos'},{title: 'Editar'}]
        },
      },
      { path: 'view/:id', component: ViewComponent,
      data: {
          title: 'Grupos',
          urls: [{title: 'Inicio', url: '/'},{title: 'Grupos'},{title: 'Ver'}]
        },
      },
      { path: 'remove/:id', component: RemoveComponent,
      data: {
          title: 'Grupos',
          urls: [{title: 'Inicio', url: '/'},{title: 'Grupos'},{title: 'Eliminar'}]
        },
      }
    ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(groupRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class GroupRoutingModule { }
