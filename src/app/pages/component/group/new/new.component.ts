import { Component, AfterViewInit,OnInit } from '@angular/core';
import { GroupService } from '../../../../services/group.service';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute,Router } from '@angular/router';
import { FormsModule,ReactiveFormsModule,Validators, FormGroup, FormArray, FormBuilder  } from '@angular/forms';
import { Group } from '../group.interface';
import { MultiSelectSearchFilter } from './search-filter.pipe';
import { IMultiSelectOption,IMultiSelectOptionUser, IMultiSelectSettings, IMultiSelectTexts } from './types';

@Component({
	selector: 'new-group',
	templateUrl: './new.component.html'
})

export class NewComponent implements OnInit {
	title:string;
	subtitle:string;
  group:any = {};
  Users: IMultiSelectOption[];
	// Settings configuration
	mySettings: IMultiSelectSettings = {
	    enableSearch: true,
	    checkedStyle: 'fontawesome',
	    buttonClasses: 'btn btn-primary btn-block  btn-md',
	    dynamicTitleMaxItems: 4,
	    displayAllSelectedText: true
	};

	// Text configuration
	myTexts: IMultiSelectTexts = {
	    checkAll: 'Selecionar Todos',
	    uncheckAll: 'Deselecionar',
	    checked: 'item seleccionado',
	    checkedPlural: 'items selecionandos',
	    searchPlaceholder: 'Buscar',
	    searchEmptyResult: 'Nada encontrado...',
	    searchNoRenderText: 'Type in search box to see results...',
	    defaultTitle: 'Selecionar',
	    allSelected: 'Todos Selecionados',
	};
	public myForm: FormGroup; // our form model

	    // we will use form builder to simplify our syntax
	constructor(private _fb: FormBuilder,
		private router: Router,
		private groupService:GroupService,
		private userService:UserService) {
		this.title = "Nuevo Grupo";

		this.userService.getUsers()
		.subscribe((users)=>{
			this.Users=users
		})
	}
	ngOnInit() {
    // we will initialize our form here
    this.myForm = this._fb.group({
						name: ['', [Validators.required, Validators.minLength(5)]],
					members: ['', [Validators.required, Validators.minLength(1)]],
           });

    }
	save(myForm: FormGroup){
		this.group.id = Date.now();
		this.group.createAt = Date.now();
		this.group.companyId = localStorage.getItem('company')
		this.group.userCreate= localStorage.getItem('uid')
		this.groupService.save(this.group);
		this.router.navigate(['group']);
	}
	initTecnicals() {
        // initialize our address
        return this._fb.group({
            name: ['', Validators.required],
            description: ['']
        });
    }
  onChange() {
    //console.log(this.cargo.appointmentsList);
  }
  onChangeu() {
    //console.log(this.cargo.evaluatorList);
  }
  changeRoute(routeValue) {
    this.router.navigate([routeValue]);
  }
}
