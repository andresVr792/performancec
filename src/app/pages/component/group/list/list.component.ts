import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { GroupService } from '../../../../services/group.service'
import { UserService } from '../../../../services/user.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from '../new/types';

@Component({
	selector: 'list-group',
	templateUrl: './list.component.html'
})

export class ListComponent {
  closeResult: string;
	title:string;
	subtitle:string;
	groups:any = {};
	Users: IMultiSelectOption[];
	id:any = null;
	// Settings configuration
	mySettings: IMultiSelectSettings = {
	    enableSearch: true,
	    checkedStyle: 'fontawesome',
	    buttonClasses: 'btn btn-primary btn-block  btn-lg',
	    dynamicTitleMaxItems: 100,
	    displayAllSelectedText: true
	};

	// Text configuration
	myTexts: IMultiSelectTexts = {
	    checkAll: 'Selecionar Todos',
	    uncheckAll: 'Deselecionar',
	    checked: 'item seleccionado',
	    checkedPlural: 'items selecionandos',
	    searchPlaceholder: 'Buscar',
	    searchEmptyResult: 'Nada encontrado...',
	    searchNoRenderText: 'Type in search box to see results...',
	    defaultTitle: 'Selecionar',
	    allSelected: 'Todos Selecionados',
	};
  constructor(
		private router: Router,
		private route: ActivatedRoute,
		private groupService:GroupService,private userService:UserService) {
		this.title = "Grupos";
		this.subtitle = "Todos los grupos.";
		this.groupService.gets()
		.subscribe(group =>{
			this.groups = group;
		});
		this.userService.getUsers()
		.subscribe((users)=>{
			this.Users=users
		});



	}
	// getUser(id){
	//
	// 	this.userService.getUser(id)
	// 	.subscribe(user =>{
	// 		this.user = user;
	// 		console.log(this.user)
	// 	});
	//
	// }

}
