import { Component, AfterViewInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { GroupService } from '../../../../services/group.service';
import { ActivatedRoute,Router } from '@angular/router';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from '../new/types';

@Component({
	selector: 'view-group',
	templateUrl: './view.component.html'
})
export class ViewComponent {
	title:string;
	subtitle:string;
  group:any = {};
  Users: IMultiSelectOption[];
	id:any = null;

  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-primary btn-block  btn-lg',
    dynamicTitleMaxItems: 100,
    displayAllSelectedText: true
  };
  myTexts: IMultiSelectTexts = {
    checkAll: 'Selecionar Todos',
    uncheckAll: 'Deselecionar',
    checked: 'item seleccionado',
    checkedPlural: 'items selecionandos',
    searchPlaceholder: 'Buscar',
    searchEmptyResult: 'Nada encontrado...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Selecionar',
    allSelected: 'Todos Selecionados',
  };

	constructor(private router: Router,private route: ActivatedRoute,private userService:UserService,private groupService:GroupService) {
		this.title = "Información del Grupo";
		this.subtitle = "Detalle de su Grupo";
		this.id = this.route.snapshot.params['id'];
		this.groupService.get(this.id)
		.subscribe((group)=>{
			this.group = group;
		});
    this.userService.getUsers()
      .subscribe((users)=>{
        this.Users=users
      });

  }
	changeRoute(routeValue) {

   this.router.navigate([routeValue]);
   // you have to check this out by passing required route value.

}

}
