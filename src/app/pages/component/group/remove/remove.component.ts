import { Component, AfterViewInit } from '@angular/core';
import { GroupService } from '../../../../services/group.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'remove-group',
	templateUrl: './remove.component.html'
})
export class RemoveComponent {
	title:string;
	subtitle:string;
  group:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private groupService: GroupService) {
		this.title = "Eliminar Group";
		this.subtitle = "Eliminar";
		this.id = this.route.snapshot.params['id'];
		this.groupService.get(this.id)
		.subscribe((group)=>{
			this.group = group;
		});
	}
	removeGroup(){
		this.groupService.remove(this.group);
		this.router.navigate(['group']);
	}
	changeRoute(routeValue) {

   this.router.navigate([routeValue]);
   // you have to check this out by passing required route value.

}


}
