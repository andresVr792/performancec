import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { GroupComponent } from './group.component';
import { HttpModule } from '@angular/http';

import { GroupRoutingModule } from './group-routing.module';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { TecnicalComponent } from './tecnical/tecnical.component';

import { GuardService } from '../../../services/guard.service';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
	imports: [
    	FormsModule,
			MultiselectDropdownModule,
    	CommonModule,
      ReactiveFormsModule,
			GroupRoutingModule,
      NgbModule.forRoot()
    ],
	declarations: [
			GroupComponent,
			EditComponent,
			NewComponent,
			ViewComponent,
			ListComponent,
			RemoveComponent,
			TecnicalComponent
	],
	providers:[
		GuardService
	]
})
export class GroupModule { }
