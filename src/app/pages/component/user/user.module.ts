import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { HttpModule } from '@angular/http';

import { UserRoutingModule } from './user-routing.module';
import { EditUserComponent } from './edit/edit.component';
import { NewUserComponent } from './new/new.component';
import { ListUserComponent } from './list/list.component';
import { ViewUserComponent } from './view/view.component';
import { RemoveUserComponent } from './remove/remove.component';

import { GuardService } from '../../../services/guard.service';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
			UserRoutingModule,
      NgbModule.forRoot()
    ],
	declarations: [
			UserComponent,
			EditUserComponent,
			NewUserComponent,
			ViewUserComponent,
			ListUserComponent,
			RemoveUserComponent
	],
	providers:[
		GuardService
	]
})
export class UserModule { }
