import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { GuardService } from '../../../services/guard.service';
import { EditUserComponent } from './edit/edit.component';
import { NewUserComponent } from './new/new.component';
import { ListUserComponent } from './list/list.component';
import { ViewUserComponent } from './view/view.component';
import { RemoveUserComponent } from './remove/remove.component';
import { UserComponent } from './user.component';

const userRoutes: Routes = [
  {
    path: 'user',
    data: {
        title: 'Usuario',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Usuario'}]
      },
    component: UserComponent,
    canActivate: [GuardService],
    children: [
      { path: '', redirectTo: 'list' , pathMatch: 'full',
      data: {
          title: 'Usuario',
          urls: [{title: 'Inicio', url: '/'},{title: 'Usuario', url: '/user'},{title: 'Lista'}]
        },
      },
      { path: 'list', component: ListUserComponent,
      data: {
          title: 'Usuario',
          urls: [{title: 'Inicio', url: '/'},{title: 'Usuario', url: '/user'},{title: 'Lista'}]
        },
    },
      { path: 'new', component: NewUserComponent,
      data: {
          title: 'Usuario',
          urls: [{title: 'Inicio', url: '/'},{title: 'Usuario', url: '/user'},{title: 'Nuevo'}]
        },
      },
      { path: 'edit/:id', component: EditUserComponent,
      data: {
          title: 'Usuario',
          urls: [{title: 'Inicio', url: '/'},{title: 'Usuario'},{title: 'Editar'}]
        },
      },
      { path: 'view/:id', component: ViewUserComponent,
      data: {
          title: 'Usuario',
          urls: [{title: 'Inicio', url: '/'},{title: 'Usuario'},{title: 'Ver'}]
        },
      },
      { path: 'remove/:id', component: RemoveUserComponent,
      data: {
          title: 'Usuario',
          urls: [{title: 'Inicio', url: '/'},{title: 'Usuario'},{title: 'Eliminar'}]
        },
      }
    ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(userRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class UserRoutingModule { }
