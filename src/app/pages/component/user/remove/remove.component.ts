import { Component, AfterViewInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'remove-user',
	templateUrl: './remove.component.html'
})
export class RemoveUserComponent {
	title:string;
	subtitle:string;
  user:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private userService:UserService) {
		this.title = "Eliminar Usuario";
		this.subtitle = "Eliminar";
		this.id = this.route.snapshot.params['id'];
		this.userService.getUser(this.id)
		.subscribe((user)=>{
			this.user = user;
		});
	}
	removeUser(){
		this.userService.removeUser(this.user);
		this.router.navigate(['user']);
	}
	changeRoute(routeValue) {

   this.router.navigate([routeValue]);
   // you have to check this out by passing required route value.

}


}
