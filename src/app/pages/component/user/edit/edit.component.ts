import { Component, AfterViewInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute,Router } from '@angular/router';
import { CompanyService } from '../../../../services/company.service';
import { RolService } from '../../../../services/rol.service';
import { CargoService } from '../../../../services/cargo.service';
@Component({
	selector: 'edit-user',
	templateUrl: './edit.component.html'
})
export class EditUserComponent {
	title:string;
	subtitle:string;
  user:any = {};
	id:any = null;
	companies:any = {};
	rols:any = {};
	cargos:any = {};

	constructor(private router: Router,
		private route: ActivatedRoute,
		private userService:UserService,
		private companyService:CompanyService,
		private rolService:RolService,
		private cargoService:CargoService) {
		this.title = "Editar Usuario";
		this.subtitle = "Edite la información de su usuario";
		this.id = this.route.snapshot.params['id'];
		this.userService.getUser(this.id)
		.subscribe((user)=>{
			this.user = user;
		});

		companyService.getCompanies()
		.subscribe(company =>{
			this.companies = company;
		});
		rolService.gets()
		.subscribe(rol =>{
			this.rols = rol;
		});
		cargoService.gets()
		.subscribe(cargo =>{
			this.cargos = cargo;
		});
	}
	editUser(){
		this.user.editAt = Date.now();
		this.userService.editUser(this.user);
		this.router.navigate(['user']);
	}
	changeRoute(routeValue) {
   this.router.navigate([routeValue]);
}


}
