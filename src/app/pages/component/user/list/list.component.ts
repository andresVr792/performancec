import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../../services/user.service'
import { CompanyService } from '../../../../services/company.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'list-user',
	templateUrl: './list.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
    .dark-modal .modal-content {
      background-color: #009efb;
      color: white;
    }
    .dark-modal .close {
      color: white;s
    }
  `]
})

export class ListUserComponent {
  closeResult: string;
	title:string;
	subtitle:string;
	users = null;
	companies = null;
	user =null;
	id:any = null;
  constructor(
		private router: Router,
		private route: ActivatedRoute,
		private userService:UserService,
		private companyService:CompanyService,
		private modalService: NgbModal,
		 private modalService2: NgbModal,) {
		this.title = "Usuario";
		this.subtitle = "Todos los Usuarios.";
		userService.getUsers()
		.subscribe(user =>{
			this.users = user;
		});
		companyService.getCompanies()
		.subscribe(company =>{
			this.companies = company;
		});
	}
	removeUser(id){
		this.user.id=id;
		this.user.editAt = Date.now();
		this.user.state =false;
		this.userService.editUser(this.user);
		this.router.navigate(['user']);

	}

  open2(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open(content) {
    this.modalService2.open(content, { windowClass: 'dark-modal' });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
