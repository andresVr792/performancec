import { Component,OnInit, AfterViewInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { CompanyService } from '../../../../services/company.service';
import { RolService } from '../../../../services/rol.service';
import { CargoService } from '../../../../services/cargo.service';
import { AuthService } from '../../../../services/auth.service';
import { ActivatedRoute,Router } from '@angular/router';
import { FormsModule,ReactiveFormsModule,Validators, FormGroup, FormArray, FormBuilder  } from '@angular/forms';

@Component({
	selector: 'new-user',
	templateUrl: './new.component.html'
})
export class NewUserComponent implements OnInit{
	title:string;
	subtitle:string;
  user:any = {};
	companies:any = {};
	rols:any = {};
	cargos:any = {};
	public myForm: FormGroup; // our form model


	constructor(private _fb: FormBuilder,
		private router: Router,
		private userService:UserService,
		private companyService:CompanyService,
		private rolService:RolService,
		private cargoService:CargoService,
		private authService:AuthService) {
		this.title = "Nueva Usuario";
		companyService.getCompanies()
		.subscribe(company =>{
			this.companies = company;
		});
		rolService.gets()
		.subscribe(rol =>{
			this.rols = rol;
		});
		cargoService.gets()
		.subscribe(cargo =>{
			this.cargos = cargo;
		});
	}
	ngOnInit() {
    // we will initialize our form here
    this.myForm = this._fb.group({
						displayName: ['', [Validators.required, Validators.minLength(5)]],
						idCard: ['', [Validators.required, Validators.minLength(10)]],
						email: ['', [Validators.required, Validators.minLength(8)]],
						company: ['', [Validators.required, Validators.minLength(1)]],
						bithDate: ['', ],
						phone: ['', [Validators.required, Validators.minLength(10)]],
						gender: ['', [Validators.required, Validators.minLength(1)]],
						rol: ['', [Validators.required, Validators.minLength(1)]],
						cargo: ['', [Validators.required, Validators.minLength(1)]],
        });
    }
	save(){
		this.user.id = Date.now();
		this.user.createAt = Date.now();
		this.user.state=true;
		this.user.companyId=localStorage.getItem('company')
		this.user.password='12345678';
		this.user.userCreate = localStorage.getItem('uid');
		this.userService.saveUser(this.user);
		this.authService.newsignup(this.user);
		this.router.navigate(['user']);
	}
	hack(val) {
  return Array.from(val);
	}
	changeRoute(routeValue) {
   this.router.navigate([routeValue]);
}

}
