import { Component, AfterViewInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'view-user',
	templateUrl: './view.component.html'
})
export class ViewUserComponent {
	title:string;
	subtitle:string;
  user:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute,private userService:UserService) {
		this.title = "Información Usuario";
		this.subtitle = "Detalle de su Usuario";
		this.id = this.route.snapshot.params['id'];
		this.userService.getUser(this.id)
		.subscribe((user)=>{
			this.user = user;
		});
	}
	changeRoute(routeValue) {

   this.router.navigate([routeValue]);
   // you have to check this out by passing required route value.

}

}
