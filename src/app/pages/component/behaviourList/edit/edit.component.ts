import { Component, AfterViewInit } from '@angular/core';
import { BehaviourListService } from '../../../../services/behaviourList.service';
import { CategoryService } from '../../../../services/category.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'edit-behaviourList',
	templateUrl: './edit.component.html'
})
export class EditComponent {
	title:string;
  categorys:any= {};
	subtitle:string;
  behaviour:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private categoryService:CategoryService, private behaviourListService:BehaviourListService) {
		this.title = "Editar Comportamiento";
		this.subtitle = "Edite la información del comportamiento de la competencia: "+localStorage.getItem('appointmentName') ;
		this.id = this.route.snapshot.params['id'];
		this.behaviourListService.get(this.id)
		.subscribe((behaviour)=>{
			this.behaviour = behaviour;
		});

    categoryService.gets()
      .subscribe(category =>{
        this.categorys = category;
      });
	}
	edit(){
		this.behaviour.editAt = Date.now();
		this.behaviourListService.edit(this.behaviour);
		this.router.navigate(['behaviourList']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
