import { Component, ViewEncapsulation } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { BehaviourListService } from '../../../../services/behaviourList.service'
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'list-behaviourList',
	templateUrl: './list.component.html',
	encapsulation: ViewEncapsulation.None,
	styles: [`
    .dark-modal .modal-content {
      background-color: #009efb;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
  `]
})

export class ListComponent {
  closeResult: string;
	title:string;
	subtitle:string;
  behaviourList = null;
  behaviourLists =null;
	id:any = null;
  constructor(private router: Router,private route: ActivatedRoute,private behaviourListService:BehaviourListService, private modalService: NgbModal, private modalService2: NgbModal) {
		this.title = 'Comportamientos';
		this.subtitle = 'Competencia: ' + localStorage.getItem('appointmentName') ;
		behaviourListService.gets()
		.subscribe(behaviourList =>{
			this.behaviourLists = behaviourList;
		});
	}
	remove(id){
		this.behaviourList.id=id;
		this.behaviourList.editAt = Date.now();
		this.behaviourList.state =false;
		this.behaviourListService.edit(this.behaviourList);
		this.router.navigate(['appointment']);

	}
  saveBehaviour(){
    this.router.navigate(['appointment']);
    localStorage.setItem('appointmentID', '0');
  }

  open2(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open(content) {
    this.modalService2.open(content, { windowClass: 'dark-modal' });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
