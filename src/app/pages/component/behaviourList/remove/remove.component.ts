import { Component, AfterViewInit } from '@angular/core';
import { BehaviourListService } from '../../../../services/behaviourList.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'remove-behaviourList',
	templateUrl: './remove.component.html'
})
export class RemoveComponent {
	title:string;
	subtitle:string;
  behaviour:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute, private behaviourListService:BehaviourListService) {
		this.title = "Eliminar Rol";
		this.subtitle = "Eliminar";
		this.id = this.route.snapshot.params['id'];
		this.behaviourListService.get(this.id)
		.subscribe((behaviour)=>{
			this.behaviour = behaviour;
		});
	}
	remove(){
		this.behaviour.editAt = Date.now();
		this.behaviour.state =false;
		this.behaviourListService.edit(this.behaviour);
		this.router.navigate(['behaviourList']);
	}
	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
