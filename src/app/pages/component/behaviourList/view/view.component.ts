import { Component, AfterViewInit } from '@angular/core';
import { BehaviourListService } from '../../../../services/behaviourList.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'view-behaviourList',
	templateUrl: './view.component.html'
})
export class ViewComponent {
	title:string;
	subtitle:string;
  behaviourLists:any = {};
	id:any = null;

	constructor(private router: Router,private route: ActivatedRoute,private behaviourListService:BehaviourListService) {
		this.title = "Información Comportamiento";
		this.subtitle = "Detalle del comportamiento";
		this.id = this.route.snapshot.params['id'];
		this.behaviourListService.get(this.id)
		.subscribe((behaviourList)=>{
			this.behaviourLists = behaviourList;
		});
	}
	changeRoute(routeValue) {
   this.router.navigate([routeValue]);
}

}
