import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { BehaviourListComponent } from './behaviourList.component';

const behaviourListRoutes: Routes = [
  {
    path: 'behaviourList',
    data: {
        title: 'BehaviourList',
        urls: [{title: 'Inicio', url: '/'},{title: 'Administración'},{title: 'Comportamientos'}]
      },
    component: BehaviourListComponent,
        children: [
          { path: '', redirectTo: 'list' , pathMatch: 'full',
          data: {
              title: 'Comportamientos',
              urls: [{title: 'Inicio', url: '/'},{title: 'Comportamientos', url: '/behaviourList'},{title: 'Lista'}]
            },
          },
          { path: 'list', component: ListComponent,
          data: {
              title: 'Comportamientos',
              urls: [{title: 'Inicio', url: '/'},{title: 'Comportamientos', url: '/behaviourList'},{title: 'Lista'}]
            },
        },
          { path: 'new', component: NewComponent,
          data: {
              title: 'Comportamientos',
              urls: [{title: 'Inicio', url: '/'},{title: 'Comportamientos', url: '/behaviourList'},{title: 'Nuevo'}]
            },
          },
          { path: 'edit/:id', component: EditComponent,
          data: {
              title: 'Comportamientos',
              urls: [{title: 'Inicio', url: '/'},{title: 'Comportamientos'},{title: 'Editar'}]
            },
          },
          { path: 'view/:id', component: ViewComponent,
          data: {
              title: 'Comportamientos',
              urls: [{title: 'Inicio', url: '/'},{title: 'Comportamientos'},{title: 'Ver'}]
            },
          },
          { path: 'remove/:id', component: RemoveComponent,
          data: {
              title: 'Comportamientos',
              urls: [{title: 'Inicio', url: '/'},{title: 'Comportamientos'},{title: 'Eliminar'}]
            },
          }
        ]
  }
];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
    	RouterModule.forChild(behaviourListRoutes)
    ],
    exports: [
      RouterModule
    ]
})
export class BehaviourListRoutingModule { }
