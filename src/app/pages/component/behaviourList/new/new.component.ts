import { Component, AfterViewInit } from '@angular/core';
import { CategoryService } from '../../../../services/category.service';
import { BehaviourListService } from '../../../../services/behaviourList.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
	selector: 'new-behaviourList',
	templateUrl: './new.component.html'
})
export class NewComponent {
	categorys:any= {};
  behaviour:any= {};
  titleBehaviour:string;
  subtitleBehaviour:string;

	constructor(private router: Router,private categoryService:CategoryService, private behaviourListService:BehaviourListService) {
		this.titleBehaviour="Nuevo Comportamiento";
    this.subtitleBehaviour="Ingrese los datos del comportamiento";
    categoryService.gets()
      .subscribe(category =>{
        this.categorys = category;
      });

	}
  saveBehaviour(){


    this.behaviour.id = Date.now();
    this.behaviour.idAppointment = localStorage.getItem('appointmentID');
    this.behaviourListService.save(this.behaviour);

    this.router.navigate(['behaviourList']);

  }

	changeRoute(routeValue) {
	 this.router.navigate([routeValue]);
	}

}
