import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { BehaviourListComponent } from './behaviourList.component';
import { HttpModule } from '@angular/http';

import { BehaviourListRoutingModule } from './behaviourList-routing.module';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
	declarations: [
			BehaviourListComponent,
			EditComponent,
			NewComponent,
			ViewComponent,
			ListComponent,
			RemoveComponent
	],
	imports: [
    	FormsModule,
    	CommonModule,
      ReactiveFormsModule,
			BehaviourListRoutingModule,
      NgbModule.forRoot()
    ],
		exports: [
			BehaviourListComponent,
			EditComponent,
			NewComponent,
			ViewComponent,
			ListComponent,
			RemoveComponent
		],
		providers:[]

})
export class BehaviourListModule { }
