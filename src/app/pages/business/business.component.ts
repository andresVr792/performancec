import { Component, AfterViewInit, } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { CompanyService } from '../../services/company.service';
import { UploadService } from '../../services/upload.service';

import * as firebase from 'firebase/app';
import 'firebase/storage';
@Component({
	selector: 'ea-business',
	templateUrl: './business.component.html'
})
export class BusinessComponent implements AfterViewInit {
	title:string;
	company:any = {};
	subtitle:string;
	url;

  constructor(
		private companyService:CompanyService,
		private uploadService:UploadService){
			this.companyService.getCompany(localStorage.getItem('company'))
		 .subscribe((company)=>{
			 this.company = company

		 })
		 this.url=localStorage.getItem('imgURL')
  }
	hack(val) {
  return Array.from(val);
	}

	ngAfterViewInit(){
	}

}
