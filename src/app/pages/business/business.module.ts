import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { BusinessComponent } from './business.component';


const routes: Routes = [{
	path: '',
	data: {
			title: 'Mi Empresa',
			urls: [{title: 'Inicio', url: '/'},{title: 'Mi Empresa'},{title: 'Perfil'}]
		},
	component: BusinessComponent
}];

@NgModule({
	imports: [
    	FormsModule,
    	CommonModule,
    	RouterModule.forChild(routes)
    ],
	declarations: [BusinessComponent]
})
export class BusinessModule { }
