import { Component, AfterViewInit } from '@angular/core';
@Component({
	selector: 'ea-starter',
	templateUrl: './starter.component.html'
})
export class StarterComponent implements AfterViewInit {
	ROLE_ADMIN:boolean =false ;
	ROLE_RRHH:boolean =false ;
	ROLE_USER:boolean =false ;
	rol:string;
	title:string;
	subtitle:string;
	
	constructor() {
		this.rol=localStorage.getItem('rol')
		this.validateRole();
	}
	validateRole(){
			if(this.rol=='ROLE_ADMIN'){
				this.ROLE_ADMIN=true;
				this.title = "Administrador";
				this.subtitle = "Bienvenidos a Performance | Coana."
			}else if(this.rol=='ROLE_RRHH'){
				this.ROLE_RRHH=true;
				this.title = "Recursos Humanos";
				this.subtitle = "Bienvenidos a Performance | Coana."
			}else if(this.rol=='ROLE_USER'){
				this.ROLE_USER=true;
				this.title = "Actividades Pendientes";
				this.subtitle = "Bienvenidos a Performance | Coana."
			}else{
				this.ROLE_USER=true;
				this.title = "Actividades Pendientes";
				this.subtitle = "Bienvenidos a Performance | Coana."
			}
		}

	ngAfterViewInit(){}
}
