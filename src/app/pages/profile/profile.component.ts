import { Component, AfterViewInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
@Component({
	selector: 'ea-profile',
	templateUrl: './profile.component.html'
})
export class ProfileComponent implements AfterViewInit {
	title:string;
	user:any = {};
	subtitle:string;

  constructor(private userService:UserService){
			this.userService.getUser(localStorage.getItem('uid'))
			.subscribe((user)=>{
				this.user = user;
			});
  }
	update(){
		this.user.editAt = Date.now();
		this.user.userEdit = localStorage.getItem('uid');
		this.userService.editUser(this.user);
		alert("Actualización correcta")
	}
	ngAfterViewInit(){}
}
