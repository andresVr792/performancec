import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandigpageComponent } from './landigpage.component';

describe('LandigpageComponent', () => {
  let component: LandigpageComponent;
  let fixture: ComponentFixture<LandigpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandigpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandigpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
