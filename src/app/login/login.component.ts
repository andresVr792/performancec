import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginParams:any = {};
  mensaje:string = "";

  constructor(private router: Router,private authService: AuthService) {
    if(localStorage.getItem('loggedIn')=='yes'){
      this.router.navigate(['/']);
    }

  }

  ngOnInit() {

  }
  login(){
    if(this.loginParams.email != null && this.loginParams.password != null){
    this.authService.login(this.loginParams);
  }else{
    this.mensaje="Usuario y/o contraseña incorrectos";
  }

  }
  facebookLogin(){
    this.authService.facebookLogin();
  }
  googleLogin(){
    this.authService.googleLogin();
  }
}
