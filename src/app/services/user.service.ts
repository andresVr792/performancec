import {Injectable} from "@angular/core";

import {AngularFireDatabase} from "angularfire2/database/database";
import { Observable } from 'rxjs/Observable';
@Injectable()

export class UserService {

  constructor(private afDB:AngularFireDatabase){

  }
  public getUsers(){
    if(localStorage.getItem('rol')=='ROLE_ADMIN'){
    return this.afDB.list('user')
  }else{
    return this.afDB.list('user',{
      query: { orderByChild: 'companyId', equalTo:  localStorage.getItem('company')}
    });
    }
  }
  public getUser(id){
    return this.afDB.object('user/'+id);
  }
  public saveUser(user){
    this.afDB.database.ref('user/'+user.id).set(user);
  }
  public editUser(user){
    this.afDB.database.ref('user/'+user.id).set(user);
  }
  public removeUser(user){
    this.afDB.database.ref('user/'+user.id).remove();
  }

}
