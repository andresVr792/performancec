import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database/database";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/switchMap';

@Injectable()

export class QualificationScaleService {
cargoWithUserList;
cargoList;
  constructor(private afDB:AngularFireDatabase){

  }

  public gets(){
    if(localStorage.getItem('rol')=='ROLE_ADMIN'){
      return this.afDB.list('qualificationScale')
    }else{
    return this.afDB.list('qualificationScale',{
      query: { orderByChild: 'companyId', equalTo:  localStorage.getItem('company')}
    });
  }
  }
  public getsR(id){
    return this.afDB.list('qualificationScale',{
      query: { orderByChild: 'id', equalTo:  id}
    });
  }
  public get(id){
    return this.afDB.object('qualificationScale/'+id);
  }


  public save(scale){
    this.afDB.database.ref('qualificationScale/'+scale.id).set(scale);
  }
  public edit(scale){
    this.afDB.database.ref('qualificationScale/'+scale.id).update(scale);
  }
  public r(scale){
    this.afDB.database.ref('qualificationScale/'+scale.id).remove();
  }
  public remove(scale){
    this.afDB.database.ref('qualificationScale/'+scale.id).remove();
  }

}
