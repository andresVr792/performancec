import {Injectable} from "@angular/core";

import {AngularFireDatabase} from "angularfire2/database/database";
import { Observable } from 'rxjs/Observable';
@Injectable()

export class CategoryService {

  constructor(private afDB:AngularFireDatabase){

  }
  public gets(){
    if(localStorage.getItem('rol')=='ROLE_ADMIN'){
      return this.afDB.list('category')
    }else{
    return this.afDB.list('category',{
      query: { orderByChild: 'companyId', equalTo:  localStorage.getItem('company')}
    });
  }
  }
  public get(id){
    return this.afDB.object('category/'+id);
  }
  public getTrue(){
    return this.afDB.list('category',{ query: {orderByChild: 'state',equalTo: "true" } });
  }

  public save(category){
    this.afDB.database.ref('category/'+category.id).set(category);
  }
  public edit(category){
    this.afDB.database.ref('category/'+category.id).set(category);
  }
  public remove(category){
    this.afDB.database.ref('category/'+category.id).remove();
  }

}
