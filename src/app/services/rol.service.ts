import {Injectable} from "@angular/core";

import {AngularFireDatabase} from "angularfire2/database/database";
import { Observable } from 'rxjs/Observable';
@Injectable()

export class RolService {

  constructor(private afDB:AngularFireDatabase){

  }
  public gets(){
    return this.afDB.list('rol');
  }
  public get(id){
    return this.afDB.object('rol/'+id);
  }

  public save(rol){
    this.afDB.database.ref('rol/'+rol.id).set(rol);
  }
  public edit(rol){
    this.afDB.database.ref('rol/'+rol.id).set(rol);
  }
  public remove(rol){
    this.afDB.database.ref('rol/'+rol.id).remove();
  }

}
