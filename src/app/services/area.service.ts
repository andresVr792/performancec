import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database/database";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/switchMap';

@Injectable()

export class AreaService {
cargoWithUserList;
cargoList;
  constructor(private afDB:AngularFireDatabase){

  }

  public gets(){
    if(localStorage.getItem('rol')=='ROLE_ADMIN'){
      return this.afDB.list('area')
    }else{
    return this.afDB.list('area',{
      query: { orderByChild: 'companyId', equalTo:  localStorage.getItem('company')}
    });
  }
  }
  public getsR(id){
    return this.afDB.list('area',{
      query: { orderByChild: 'id', equalTo:  id}
    });
  }
  public get(id){
    return this.afDB.object('area/'+id);
  }


  public save(area){
    this.afDB.database.ref('area/'+area.id).set(area);
  }
  public edit(area){
    this.afDB.database.ref('area/'+area.id).update(area);
  }
  public remove(area){
    this.afDB.database.ref('area/'+area.id).remove();
  }

}
