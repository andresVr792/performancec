import {Injectable} from "@angular/core";

import {AngularFireDatabase} from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
@Injectable()

export class CompanyService {

  constructor(private afDB:AngularFireDatabase){

  }
  public getCompanies(){
    if(localStorage.getItem('rol')=='ROLE_ADMIN'){
    return this.afDB.list('company/');
  }else{
    return this.afDB.list('company',{
      query: { orderByChild: 'companyId', equalTo:  localStorage.getItem('company')}
    });
  }
  }
  public getCompany(id){
    return this.afDB.object('company/'+id);
  }
  public getCompanyByName(name){
    return this.afDB.list('company',{
      query: { orderByChild: 'name', equalTo:  name}
    });
  }

  public saveCompany(company){
    this.afDB.database.ref('company/'+company.id).set(company);
  }
  public editCompany(company){
    this.afDB.database.ref('company/'+company.id).set(company);
  }
  public removeCompany(company){
    this.afDB.database.ref('company/'+company.id).remove();
  }

}
