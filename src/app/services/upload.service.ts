import {Injectable} from "@angular/core";
import { Upload } from '../model/upload';
import {AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import 'firebase/storage';

@Injectable()


export class UploadService {

  basePath = 'uploads';

  constructor(private afDB:AngularFireDatabase){

  }
  public gets(){
    return this.afDB.list('upload/');
  }
  public get(id){
    return this.afDB.object('upload/'+id);
  }
  deleteUpload(upload: Upload) {
      this.deleteFileData(upload.id)
      .then( () => {
        this.deleteFileStorage(upload.name)
      })
      .catch(error => console.log(error))
    }
    public getImg(name){
      const storageRef = firebase.storage().ref().child('uploads/'+name);
      storageRef.getDownloadURL().then(url =>
          localStorage.setItem('imgURL',url)
      );
    }


  pushUpload(upload) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${upload.file.name}`).put(upload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) =>  {
        // upload in progress
        const snap = snapshot as firebase.storage.UploadTaskSnapshot
        upload.progress = (snap.bytesTransferred / snap.totalBytes) * 100
      },
      (error) => {
        // upload failed
        console.log(error)
      },
      () => {
        // upload success
        upload.url = uploadTask.snapshot.downloadURL
        upload.name = upload.file.name
        this.saveFileData(upload)
        return upload
      }
    );
  }

  public edit(upload){
    this.afDB.database.ref('upload/'+upload.id).set(upload);
  }
  public remove(upload){
    this.afDB.database.ref('upload/'+upload.id).remove();
  }
  // Writes the file details to the realtime db
 private saveFileData(upload) {
   this.afDB.list(`${this.basePath}/`).push(upload);
 }
 // Writes the file details to the realtime db
 private deleteFileData(id: string) {
   return this.afDB.list(`${this.basePath}/`).remove(id);
 }
 // Firebase files must have unique names in their respective storage dir
  // So the name serves as a unique key
  private deleteFileStorage(name: string) {
    const storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/${name}`).delete()
  }

}
