import {Injectable} from "@angular/core";
import {AngularFireAuth} from "angularfire2/auth/auth";
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute,Router,CanActivate  } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable()

export class GuardService implements CanActivate{

  loggedIn = false;
	constructor(private router: Router,
    private authService:AuthService) {

  }
  canActivate(){
    this.authService.isLogged()
    .subscribe((result)=>{
      if(result && result.uid){
        this.loggedIn = true;
      }else{
        this.loggedIn = false;
        this.router.navigate(['/login']);
      }
    }),(error)=>{
      this.loggedIn = false;
      this.router.navigate(['/login']);
    }

    return this.loggedIn;
  }
}
