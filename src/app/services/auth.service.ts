import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database/database";
import {AngularFireAuth} from "angularfire2/auth/auth";
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
@Injectable()

export class AuthService {

  constructor(
    private angularFireAuth:AngularFireAuth,
    private afDB:AngularFireDatabase,
    private router:Router){
    this.isLogged();
  }
  //// Social Auth ////

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider()
    return this.socialSignIn(provider);
  }

  facebookLogin() {
    const provider = new firebase.auth.FacebookAuthProvider()
    return this.socialSignIn(provider);
  }
  private socialSignIn(provider) {
  return this.angularFireAuth.auth.signInWithPopup(provider)
    .then((credential) => {
      this.updateUserData(credential.user);
      this.guardarUid(credential.user);
      alert('Usuario logeado');
      this.router.navigate(['']);
    })
    .catch(error => console.log(error));
}

  public login(loginParams){
    return this.angularFireAuth.auth.signInWithEmailAndPassword(loginParams.email,loginParams.password)
    .then((response) => {
      this.guardarUid(response);
      alert('Usuario logeado');
      this.router.navigate(['']);
    })
    .catch((error)=>{
      console.log(error)
      alert('Usuario o Contraseña incorrectos');
    });
  }
public signup(signupParams){
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(signupParams.email,signupParams.password)
    .then((response) => {
      this.updateUserMail(response);
      this.guardarUid(response);
      alert('Usuario registrado');
      this.router.navigate(['']);
    })
    .catch((error)=>{
      alert('error el correo ya existe');
    });
  }
  public newsignup(user){
      return this.angularFireAuth.auth.createUserWithEmailAndPassword(user.email,user.password)
      .then((response) => {
        alert('Usuario registrado');
      })
      .catch((error)=>{
        alert('error el correo ya existe');
      });
    }
  public isLogged(){
    return this.angularFireAuth.authState;
  }
  public logout(){
    localStorage.clear();
    this.angularFireAuth.auth.signOut();
  }
  public getUser(){
    return this.angularFireAuth.auth;
  }
  public recover(recoverParams){
    this.angularFireAuth.auth.sendPasswordResetEmail(recoverParams.email).then((response) => {
      alert('Se envio un correo para cambiar su contraseña');
      this.router.navigate(['login']);
    })
    .catch((error)=>{
      alert('un error ah ocurrido');
    });
  }
  ///helpers
  private updateUserData(user){
    var usuario:any;
  this.afDB.object('user/'+user.uid).
  subscribe(user=>{
    usuario = user;
  })
        var data = {
          id: user.uid,
          uid: user.uid,
          email: user.email,
          displayName: user.displayName,
          photoURL: user.photoURL,
          companyId: localStorage.getItem('companyId')
        }
        this.afDB.database.ref('user/'+user.uid).update(data);
  }
  private updateUserMail(user){
    var photo = "https://firebasestorage.googleapis.com/v0/b/performancecoana.appspot.com/o/uploads%2Fif_profle_1055000%20(1).png?alt=media&token=ccc18c1a-cbbb-47a6-8c98-b3caa9343453";
        let data = {
          id: user.uid,
          uid: user.uid,
          email: user.email,
          displayName: "USUARIO",
          photoURL: photo,
          rol:"ROLE_USER",
          companyId: localStorage.getItem('companyId')

        }
        this.afDB.database.ref('user/'+user.uid).set(data);
  }
  public getUserLogin(id){
    if(this.afDB.object('user/'+id)!=null){
    return true;
    }else{
      return false;
    }
  }
  ///UID STORAGE
  private guardarUid(user){
    localStorage.setItem('uid',user.uid);

  }
}
