import {Injectable} from "@angular/core";

import {AngularFireDatabase} from "angularfire2/database/database";
import { Observable } from 'rxjs/Observable';
@Injectable()

export class AppointmentService {

  constructor(private afDB:AngularFireDatabase){

  }
  public gets(){
    return this.afDB.list('appointment/',{
      query: { orderByChild: 'companyId', equalTo:  localStorage.getItem('company')}
    });
  }
  public get(id){
    return this.afDB.object('appointment/'+id);
  }


  public save(appointment){
    this.afDB.database.ref('appointment/'+appointment.id).set(appointment);
  }
  public edit(appointment){
    this.afDB.database.ref('appointment/'+appointment.id).set(appointment);
  }
  public remove(appointment){
    this.afDB.database.ref('appointment/'+appointment.id).remove();
  }

}
