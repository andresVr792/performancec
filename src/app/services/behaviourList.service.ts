import {Injectable} from "@angular/core";

import {AngularFireDatabase} from "angularfire2/database/database";
import { Observable } from 'rxjs/Observable';
@Injectable()

export class BehaviourListService {

  constructor(private afDB:AngularFireDatabase){

  }
  public gets(){
    return this.afDB.list('behaviourList',{
      query: {orderByChild: 'idAppointment', equalTo: localStorage.getItem('appointmentID') } });
  }
  public get(id){
    return this.afDB.object('behaviourList/'+id);
  }


  public save(behaviourList){
    this.afDB.database.ref('behaviourList/'+behaviourList.id).set(behaviourList);
  }
  public edit(behaviourList){
    this.afDB.database.ref('behaviourList/'+behaviourList.id).set(behaviourList);
  }
  public remove(behaviourList){
    this.afDB.database.ref('behaviourList/'+behaviourList.id).remove();
  }

}
