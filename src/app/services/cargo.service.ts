import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database/database";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/switchMap';

@Injectable()

export class CargoService {
cargoWithUserList;
cargoList;
  constructor(private afDB:AngularFireDatabase){

  }
  // public getJoin(){
  //   this.cargoWithUserList = this.cargoList
  //   .switchMap(cargos => {
  //     let userObservables = cargos.map(cargo => this.afDB
  //       .object(`user/${cargo.id}`).first()
  //     );
  //     return userObservables.length === 0 ?
  //     Observable.of(cargos) :
  //     Observable.forkJoin(...userObservables, (...users) => {
  //       cargos.forkEach((cargo,index)=>{
  //         cargos.user = users[index].displayName;
  //       });
  //       return cargos;
  //     })
  //   });
  // }
  public getAllUsers(){
    return this.afDB
        .list(`cargo`)
        .mergeMap((evaluators) => {
            // The array of tours is going to be mapped to an observable,
            // so mergeMap is used.
            return Observable.forkJoin(
                // Map the tours to the array of observables that are to
                // be joined. Note that forkJoin requires the observables
                // to complete, so first is used.
                evaluators.map((evaluator, index) => this.afDB
                    .object(`user/${index}`)
                    .first()
                ),
                // Use forkJoin's results selector to match up the result
                // values with the tours.
                (...values) => {
                    evaluators.forEach((evaluator, index) => {
                      evaluator.name = values[index];
                    });
                    return evaluators;

                }
            );
        });
  }
  public gets(){
    if(localStorage.getItem('rol')=='ROLE_ADMIN'){
      return this.afDB.list('cargo')
    }else{
    return this.afDB.list('cargo',{
      query: { orderByChild: 'companyId', equalTo:  localStorage.getItem('company')}
    });
  }
  }
  public getsR(id){
    return this.afDB.list('cargo',{
      query: { orderByChild: 'id', equalTo:  id}
    });
  }
  public get(id){
    return this.afDB.object('cargo/'+id);
  }


  public save(cargo){
    this.afDB.database.ref('cargo/'+cargo.id).set(cargo);
  }
  public edit(cargo){
    this.afDB.database.ref('cargo/'+cargo.id).update(cargo);
  }
  public remove(cargo){
    this.afDB.database.ref('cargo/'+cargo.id).remove();
  }

}
