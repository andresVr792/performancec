import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database/database";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/switchMap';

@Injectable()

export class GroupService {
cargoWithUserList;
cargoList;
  constructor(private afDB:AngularFireDatabase){

  }

  public getAllUsers(id){
    return this.afDB
        .list(`cargo`)
        .mergeMap((evaluators) => {
            // The array of tours is going to be mapped to an observable,
            // so mergeMap is used.
            return Observable.forkJoin(
                // Map the tours to the array of observables that are to
                // be joined. Note that forkJoin requires the observables
                // to complete, so first is used.
                evaluators.map((evaluator, index) => this.afDB
                    .object(`user/${index}`)
                    .first()
                ),
                // Use forkJoin's results selector to match up the result
                // values with the tours.
                (...values) => {
                    evaluators.forEach((evaluator, index) => {
                      evaluator.name = values[index];
                    });
                    return evaluators;
                }
            );
        });
  }
  public gets(){
    if(localStorage.getItem('rol')=='ROLE_ADMIN'){
      return this.afDB.list('group')
    }else{
    return this.afDB.list('group',{
      query: { orderByChild: 'companyId', equalTo:  localStorage.getItem('company')}
    });
  }
  }
  public getsR(id){
    return this.afDB.list('group',{
      query: { orderByChild: 'id', equalTo:  id}
    });
  }
  public get(id){
    return this.afDB.object('group/'+id);
  }


  public save(group){
    this.afDB.database.ref('group/'+ group.id).set(group);
  }
  public edit(group){
    this.afDB.database.ref('group/'+ group.id).update(group);
  }
  public remove(group){
    this.afDB.database.ref('group/'+ group.id).remove();
  }

}
