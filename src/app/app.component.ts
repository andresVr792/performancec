import { Component,OnInit, OnDestroy} from '@angular/core';
import { AuthService } from './services/auth.service';
import { CompanyService } from './services/company.service';
import { UserService } from './services/user.service';
import { UploadService } from './services/upload.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit,OnDestroy{
  title = 'app';
  loggedIn = false;
  companies:any={};
  url;
  subscription;
	constructor(private router: Router,
    private authService:AuthService,
    private companyService:CompanyService,
    private userService:UserService,
    private uploadService:UploadService){


     this.companyService.getCompany(localStorage.getItem('company'))
    .subscribe((company)=>{
      this.companies = company
      this.uploadService.getImg(this.companies.imgName)
    })

    this.subscription = this.authService.isLogged()
    .subscribe((result)=>{
      if(result && result.uid){
        this.loggedIn = true;
        localStorage.setItem('loggedIn','yes')

      }else{
        this.loggedIn = false;
        this.router.navigate(['login']);
        localStorage.setItem('loggedIn','no')

      }
    }),(error)=>{
      this.loggedIn = false;
      this.router.navigate(['login']);
      localStorage.setItem('loggedIn','no')

    }
  }
  ngOnInit(){


  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
}

}
