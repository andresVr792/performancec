import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent {
  signupParams:any ={};
  mensaje:string = "";


  constructor(private router: Router,
    private authService: AuthService) {
    if(localStorage.getItem('loggedIn')=='yes'){
      this.router.navigate(['/']);
    }
  }

  signup(){
    if(this.signupParams.email != null && this.signupParams.password != null){
    this.authService.signup(this.signupParams);

  }else{
    this.mensaje="Usuario y/o contraseña incorrectos";
  }
  }

}
